module gitlab.com/gitlab-org/incubation-engineering/apm/apm

go 1.16

require (
	github.com/ClickHouse/clickhouse-go v1.4.8
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/go-playground/locales v0.14.0
	github.com/go-playground/universal-translator v0.18.0
	github.com/go-playground/validator/v10 v10.9.0
	github.com/go-redis/redis/v8 v8.11.4
	github.com/golang-migrate/migrate/v4 v4.15.0
	github.com/golangci/golangci-lint v1.42.1
	github.com/gorilla/mux v1.8.0
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/jmoiron/sqlx v1.3.4
	github.com/rs/zerolog v1.25.0 // indirect
	github.com/spf13/viper v1.9.0 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/urfave/cli/v2 v2.3.0
	github.com/vektra/mockery/v2 v2.9.4
	gitlab.com/gitlab-org/labkit v1.10.0
	go.uber.org/atomic v1.9.0 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/mod v0.5.1 // indirect
	golang.org/x/sys v0.0.0-20211025201205-69cdffdb9359 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.7 // indirect
)
