**GitLab have acquired opstrace** - https://venturebeat.com/2021/12/14/gitlab-acquires-open-source-observability-distribution-opstrace/.

The APM SEG has been folded into the opstrace project here - https://gitlab.com/gitlab-org/opstrace. We are continuing to build on ClickHouse for traces and logs.

# Monitor APM Solution Project

This project contains the proof of concept APM solution as well as a holding space for any issues.

Read more about the [Monitor APM Single-Engineer Group](https://about.gitlab.com/handbook/engineering/incubation/monitor-apm/) and the [Incubation Engineering Department](https://about.gitlab.com/handbook/engineering/incubation/).

## Goals

Storage, visualisation and management of APM (Application Performance Management) data. This also falls under the umbrella of observability, although this has become an overloaded term.

We will achieve this by:
- Using existing open source agents to capture infrastructure metrics, app logs and traces.
- Store APM data in a flexible high performance and scalable data store that will allow complex queries.
- Integrate with GitLab projects and environments (optionally) to store APM data against projects/environments allowing deep linking between data sources.
- Allow visualisation of data via well known tools such as Grafana.

Longer term we will look into alerting, incident creation and SLO management using this data source.

## Contribute

See [CONTRIBUTING.md](./CONTRIBUTING.md).

Feel free to create issues for ideas in this project, or comment on existing ones. For new issues you'll find the relevant tags are applied by default.

Merge requests for particular issues are always welcome. Please contribute!

### Internally

Visit the `#incubation-eng` Slack channel (internal) and start a conversation.

Feel free to set up a coffee chat with any SEG.

## Development

This is primarily a Golang codebase targetting Kubernetes as a runtime environment.
### Dependencies

We're using [http://asdf-vm.com/guide/introduction.html#how-it-works](asdf) to manage project specific dependencies.

If you'd rather not use that, see [.tool-versions](./.tool-versions) for the relevant tools and versions.

Building container images locally for `minikube` requires a `docker` CLI compatible build system (e.g. [docker](https://docs.docker.com/get-docker/) or [podman](https://podman.io/getting-started/)).

### Building

`make build` will build the `cmd` binaries and place them in `bin/`.

### Unit Tests

`make test` runs Go unit tests.

### Integration Tests

`make integration-test` runs Go integration tests.

Integration tests use build tag `integration` and test names end in `Integration`.
This allows running them separately from unit tests while keeping the tests alongside relevant code.

### Local Minikube dev

`make minikube-start` will create a Minikube cluster and install the ClickHouse operator.

`make skaffold-run` uses Skaffold to build the container images and deploy the Helm chart into minikube.

## Further reading

More details can be `/docs` folder.
