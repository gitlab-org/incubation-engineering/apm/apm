# Migration Strategy

ClickHouse doesn't have too many options for well supported migration systems.

We're trying https://github.com/golang-migrate/migrate as this has built in support for ClickHouse and provides a simple solution.

Follow [GitLab Migrations guidance](https://docs.gitlab.com/ee/development/database/) for strategies and conventions where this is appropriate. Not all things apply, given this is Rails migration and PostgreSQL focussed.

Our migrations rely on [Replication Macros](https://github.com/Altinity/clickhouse-operator/blob/master/docs/replication_setup.md) from the Altinity ClickHouse operator.
The database must also have `default_replica_path` and `default_replica_name` set in the config so we don't have to specify this for all replicated table types.

## Creating Migrations

See `make migrate-create` rule in the `Makefile`.

Migration files are created in `scripts/migrations` and are in the format `20211005204753_migration_name.down.sql` and `20211005204753_migration_name.up.sql`.

The timestamp prefix is preferred over a number to minimise potential conflicts.

You must implement the up and down migrations such that the migration can be successfully rolled back if needed.

Multiple SQL statements can be in each file if separated by a `;` and newline.

ClickHouse macros can be used, also the variables `$DATABASE` and `$CLUSTER` are substituted for the target database name and cluster name respectively.
This is useful when specifying distributed table engines where the database and cluster must be specified.

## Running Migrations

To run all migrations against a local database, use `make migrate-up`. Use the `migrate` command directly for more specific control.

Migrations are automatically run in Kubernetes via [Helm Chart Hooks](https://helm.sh/docs/topics/charts_hooks/).

Various hooks exist around helm commands, here's how we use them:

| Helm Command | Hook | Description |
| ------------ | ---- | ----------- |
| `helm install` | `post-install` | After install the migrations run against the target database.
| `helm upgrade` | `pre-ugprade` | Store the current migration version for rollbacks, then run the latest migrations against the database.
| `helm rollback` | `pre-rollback` | Get the target migration and rollback to this migration

Between upgrades any application changes will need to be first forwards compatible with the migration so that pods don't fail as the migration runs. First release the forward compatible code changes, then the migrations, or expect downtime/internal server errors.

It would be really great if we could use helm `pre-rollback` with resources defined via `{{.Release.Revision}}` to store migration state. Unfortunately, `rollback` hooks in Helm don't work the way one might expect, see this issue - https://github.com/helm/helm/issues/5825.

Also `.Release.Revision` increments for both upgrades and rollbacks, so isn't particularly useful for using as a marker of history beyond a single rollback.

## Method

Migrations should be added to `./scripts/migrations` via `make migrate-up`. The migrations folder must be copied into the `gitlab-apm` helm chart, this happens when any of the `make` commands related to `helm` or `skaffold` are run.

Migration sql files are deployed as a ConfigMap with a release. Each revision can therefore have the specific migrations attached to it. When rolling back the older relevant revisions are still present.

`post-install` and `pre-upgrade` hooks create a "state" configmap with an attached revision, in both the title and as a label, `migration-state-revision: "{{ .Release.Revision }}"`. See [migrations-state-configmap.yaml](../charts/gitlab-apm/templates/migrations-state-configmap.yaml) for the details of this ConfigMap.

On install/upgrade a job runs with the migrations against the DB. The state configmap is populated with `migrated_from` and `migrated_to` values. This acts as a running history of the migrations.

`pre-rollback` the rollback Job mounts the current migrations and the latest migration state configmap. These are used to migrate down to the `migrated_from` version. On success, this unique state configmap is deleted as it is no longer relevant to the cluster history/state.