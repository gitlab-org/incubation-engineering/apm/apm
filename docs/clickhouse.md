# ClickHouse

We're using ClickHouse as a database for APM data (metrics, logs, traces etc.).

For deployment we're using the [Altinity ClickHouse operator](https://github.com/Altinity/clickhouse-operator/).

See the [helm values.yaml](../deploy/charts/gitlab-apm/values.yaml) for the ClickHouse configuration.
The values are configured for Minikube usage by default.

## Requirements

The ClickHouse cluster config must have the following defined for our migrations to work:
- `default_replica_path` - default replica path used so we can omit from all definitions (see https://github.com/Altinity/clickhouse-operator/blob/master/docs/replication_setup.md).
- `default_replica_name` - default replica name used so we can omit from definitions (see also https://clickhouse.com/docs/en/engines/table-engines/mergetree-family/replication/).

## Metrics schema

`metrics_local` is a replicated merge-tree table on a ClickHouse cluster.

```sql
CREATE TABLE apm.metrics_local ON CLUSTER `{cluster}`
(
    timestamp        DateTime('UTC') CODEC(DoubleDelta, LZ4),
    host             LowCardinality(String),
    measurement      LowCardinality(String),
    project_id       Int64,
    environment_id   Int64,
    `fields.name`    Array(LowCardinality(String)),
    `fields.value`   Array(Float64) CODEC(Gorilla, LZ4),
    tags Nested(
        key LowCardinality(String),
        value LowCardinality(String)
    )
) ENGINE = ReplicatedMergeTree
PARTITION BY toYYYYMMDD(timestamp)
ORDER BY (project_id, environment_id, measurement, host, timestamp);
```

`measurement` represents a specific measurement with fields and tag on a particular `host`.

`project_id` and `environment_id` are references to a specific GitLab project/environment. Environment is zero when no associated environment is required.

`fields` and `tags` are nested array structures that allows for flexible key-value pairs to be added to a measurement.

Example row for measurement `system.cpu`:

```
 2021-11-09 14:53:34 │ gitlab-apm │ system.cpu │ 28676660 │ 0 │ ['context_switches','guest','idle','interrupt','iowait','num_cores','stolen','system','user'] │ [533113,0,91.34603307515775,0.7410508687462347,0.04186728072011893,16,0,2.5287837554956925,6.0833158886339005] │ [] │ []
```

Here host `gitlab-apm` has a measurement for `system.cpu` in project `28676660` (no environment) with various field name/values. No tags are present.