# DataDog Agent/API Support

The project will support the DataDog agent for metrics, traces and logs.

In `development` mode (see [values.yaml](../deploy/charts/gitlab-apm/values.yaml])) the datadog agent, kube-state-metrics and cluster agent are installed.
This allows for local testing of the agent.

## API Key

The API key for the datadog agent can be specified in the agent config either in the agent yaml or Helm chart.
See `api_key` in [config yaml](https://github.com/DataDog/datadog-agent/blob/main/pkg/config/config_template.yaml) and 
`datadog.apiKey` in [Helm chart values yaml](https://github.com/DataDog/helm-charts/blob/main/charts/datadog/values.yaml).

The API key format is `<api key>:<GitLab project ID>:<GitLab environment ID>` where the environment ID is optional.
See [GitLab integration doc](./gitlab.md) for more details.

## AuthNZ

The validate endpoint just verifies the api key:

| Route | Methods | Responses |
| ----- | ------- | --------- |
| /api/v1/validate | GET | 200, 403

`validate` takes the header `DD-API-KEY` which is validated against the GitLab `/version` endpoint to check the actual key component is valid.

## Metrics

We aim to support the [Metrics Submit API](https://docs.datadoghq.com/api/latest/metrics/#submit-metrics), 
this is used by the DataDog agent directly to submit metrics and can be used programmatically quite easily (e.g. via `curl` or any http library).

| Route | Methods | Responses |
| ----- | ------- | --------- |
| /api/v1/series | POST | 202, 400, 403, 408, 413

Example POST JSON body for series:
```json
{
  "series": [
    {
      "host": "test.example.com",
      "interval": 20,
      "metric": "system.load.1",
      "points": [
        [
          1575317847,
          0.5
        ]
      ],
      "tags": [
        "environment:test"
      ],
      "type": "rate"
    },
    {
      "host": "test.example.com",
      "interval": 20,
      "metric": "system.load.5",
      "points": [
        [
          1575317847,
          0.25
        ]
      ],
      "tags": [
        "environment:test"
      ],
      "type": "rate"
    }
  ]
}
```

In the above body, `points` is always a 2D array of tuples, the first value being the unix timestamp, the second value being the metric value.

### Custom Metrics

We try to follow the validation rules defined in [Custom Metrics](https://docs.datadoghq.com/metrics/custom_metrics) to decide how we validate things in this endpoint.

Validation rules:
- Host is required.
- Metric name is required and must start with a letter, only ASCII alphanumerics, underscores and periods, unicode not supported. Must not exceed 200 characters.
- Metric value can be any 64-bit floating point number.
- Timestamp is assumed to unix time integer, within range -1hr to +10min.

### Conversion

We convert the series data structure to our internal measurement structure.

`host` is trimmed of whitespace.

`timestamps` are converted from the unix seconds value into a database compatible UTC timestamp.

`values` are assumed to be 64-bit floats and stored as such.

`tags` are trimmed of whitespace and converted to key value pairs, splitting on the first `:` character.

`type` is ignored.

`interval` is ignored.

`metric` names are split into a measurement name and field name, either using the last `.` character (if the name has any `.` characters) or using the last `_` (if the name has no `.` characters and at least one `_` character).

Measurements are grouped together by unique sets of `(host, timestamp, measurement, tags)`, fields for a particular measurement become key-value pairs in the data structure.

In Go, the above JSON body becomes:

```go
Measurement {
    Timestamp:   time.Unix(1575317847, 0),
    Host:        "test.example.com",
    Measurement: "system.load",
    Fields:      map[string]float64{
        "1": 0.5,
        "5": 0.25,
    },
    Tags:        map[string]string{
        "environment": "test"
    },
}
```

### Limitations

- [Metrics Types](https://docs.datadoghq.com/metrics/types/) are not stored in the database, so this metadata can't be used to inform visualisation.
  This reduces complexity of having to define this upfront, or indeed modify the type of metric if it changes. Everything is a 64-bit float in our database.
- [Metrics Units](https://docs.datadoghq.com/metrics/units/) are not explicitly handled. It is assumed the user can handle any necessary conversions.

## Checks

The datadog agent can send service "checks" (e.g. uptime), see https://docs.datadoghq.com/api/latest/service-checks/.

We implement a stub of this API that stores none of the check data but the api key is validated against the session.

| Route | Methods | Responses |
| ----- | ------- | --------- |
| /api/v1/check_run | POST | 202, 400, 403, 408, 413

## Intake

The data agent sends status info to an undocumented `/intake/` endpoint which is not under `/api/v1`.

This endpoint takes various different models and also takes any global tags the agent is set up to provide initially.

We implement a stub of this API that stores no data but does validate the api key.

| Route | Methods | Responses |
| ----- | ------- | --------- |
| /intake/ | POST | 200, 403