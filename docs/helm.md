# Helm Chart

The project can be installed via the Helm chart at `../deploy/charts/gitlab-apm`.

See the [values.yaml](../deploy/charts/gitlab-apm/values.yaml) file for the defaults.

This chart is validated in CI and pushed to the GitLab OCI registry.

## Development Mode

Development mode is enabled by default.

Development mode installs all dependencies including the GitLab stub service.

## Chart Dependencies

See [Chart.yaml](../deploy/charts/gitlab-apm/Chart.yaml) for chart dependencies and conditions.

Enabled by default (use `enabled` for each sub-chart to turn off):
- Redis, for Gateway session storage.

Enabled in development (use `enabled` flag for each sub-chart otherwise):
- DataDog agent, for local testing against our gateway.
- Grafana, datasource configured for ClickHouse.
- ClickHouse installation, single replica.
- Zookeeper, for ClickHouse replication.