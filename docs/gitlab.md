# GitLab Project Integration

Requests against the gateway to store data will need an API key that will be validated against a GitLab instance.

The gateway requires the incoming APM data to identify a project ID and optionally environment ID to store alongside data.

## DataDog Agent Key Format

The agent is configured with an API key (see [DataDog doc](./datadog.md) for details of how), which will be an existing key in the users GitLab (access token, project token etc.)

The API key must also at least identify a project and optionally an environment that the agent is to store data against.

The API key is in the format `<GitLab API Key>:<GitLab Project ID>[:GitLab Environment ID]`.

Taking these values as an example:
- GitLab api key: `abcd-91ef`
- Project ID: `27`
- Environment ID: `2`

The API key format would be `abcd-91ef:27:2`.

Environment IDs are optional (you may not use an environment in GitLab). Ignoring environment `abcd-91ef:27` is also valid.

Additionally, ID "slugs" or paths are also valid in the key, e.g. `abcd-91ef:my_namespace/my_project:my_environment`


## GitLab API requests

Projects will be retrieved based on the provided API key and project ID. The API key must have at least `developer` level access in the project.

- Basic api token validation can be done via  - https://docs.gitlab.com/ee/api/version.html
- Projects can be retrieved like so - https://docs.gitlab.com/ee/api/projects.html#get-single-project
- Environments can be accessed like so - https://docs.gitlab.com/ee/api/environments.html#get-a-specific-environment

Example sequence diagram for a hypothetical agent:

```mermaid
sequenceDiagram
  participant Agent
  participant Gateway
  participant GitLab
  Agent->>Gateway: Post series with invalid api key or invalid project ID
  Gateway->>GitLab: Get project with api key
  GitLab-->>Gateway: 401 Unauthorized
  Gateway-->>Agent: 401 Unauthorized

  Agent->>Gateway: Post series with valid key for project (environment optional)
  Gateway->>GitLab: Get project with api key
  GitLab-->>Gateway: 200 project JSON
  alt token is not dev level
    Gateway-->>Agent: 401 Unauthorized
  else token is dev level
    opt Validate environment ID
      Gateway->>GitLab: get environments in project
      GitLab-->>Gateway: 200 content or 404 if not exists
    end
    Gateway-->>Agent: 200
  end
```

In the project response, we get relevant permissions in this attribute:
```
"permissions": {
  "project_access": {
    "access_level": 30,
    "notification_level": 3
  },
  "group_access": null
}
```

## Local Dev Stub

A Golang service to stub the GitLab projects and environments ID APIs is included in the project, see `./cmd/gtlb-stub`.

The stub provides endpoints for: 
- `/projects/{id}` - get projects by ID.
- `/projects/{id}/environments` - get environments for a project.
- `/version` - get version, used to validate tokens.

Project/environment IDs are hard coded in the service, like so:

| Project ID | Environment ID | Status | Access Level |
| ---------- | -------------- | ------ | ------------ |
| 1 | 1 | 200 | Developer |