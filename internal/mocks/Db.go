// Code generated by mockery v2.9.4. DO NOT EDIT.

package mocks

import (
	mock "github.com/stretchr/testify/mock"
	metrics "gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/metrics"
)

// Db is an autogenerated mock type for the Db type
type Db struct {
	mock.Mock
}

// Begin provides a mock function with given fields:
func (_m *Db) Begin() (metrics.Tx, error) {
	ret := _m.Called()

	var r0 metrics.Tx
	if rf, ok := ret.Get(0).(func() metrics.Tx); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(metrics.Tx)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Close provides a mock function with given fields:
func (_m *Db) Close() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Exec provides a mock function with given fields: query, args
func (_m *Db) Exec(query string, args ...interface{}) error {
	var _ca []interface{}
	_ca = append(_ca, query)
	_ca = append(_ca, args...)
	ret := _m.Called(_ca...)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, ...interface{}) error); ok {
		r0 = rf(query, args...)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// NamedSelect provides a mock function with given fields: dest, query, fields
func (_m *Db) NamedSelect(dest interface{}, query string, fields interface{}) error {
	ret := _m.Called(dest, query, fields)

	var r0 error
	if rf, ok := ret.Get(0).(func(interface{}, string, interface{}) error); ok {
		r0 = rf(dest, query, fields)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Ping provides a mock function with given fields:
func (_m *Db) Ping() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// PrepareNamed provides a mock function with given fields: query
func (_m *Db) PrepareNamed(query string) (metrics.Statement, error) {
	ret := _m.Called(query)

	var r0 metrics.Statement
	if rf, ok := ret.Get(0).(func(string) metrics.Statement); ok {
		r0 = rf(query)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(metrics.Statement)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(query)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
