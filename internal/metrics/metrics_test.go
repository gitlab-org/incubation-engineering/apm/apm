package metrics_test

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/metrics"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/mocks"
)

func Test_store_Save(t *testing.T) {
	t0 := time.Now()
	type fields struct {
		Db        *mocks.Db
		assertFun func(*testing.T)
	}
	type args struct {
		measurements []metrics.Measurement
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"empty measurements does not start transaction",
			fields{
				Db: &mocks.Db{},
			},
			args{
				nil,
			},
			false,
		},
		{
			"measurement must have non-zero timestamp",
			fields{
				Db: &mocks.Db{},
			},
			args{
				[]metrics.Measurement{
					{
						Timestamp: time.Time{},
					},
				},
			},
			true,
		},
		{
			"measurement must have non-whitespace host",
			fields{
				Db: &mocks.Db{},
			},
			args{
				[]metrics.Measurement{
					{
						Timestamp: time.Now(),
						Host:      "\t \r   ",
					},
				},
			},
			true,
		},
		{
			"measurement must have non-whitespace measurement",
			fields{
				Db: &mocks.Db{},
			},
			args{
				[]metrics.Measurement{
					{
						Timestamp:   time.Now(),
						Host:        "host",
						Measurement: "",
					},
				},
			},
			true,
		},
		{
			"measurement must have fields",
			fields{
				Db: &mocks.Db{},
			},
			args{
				[]metrics.Measurement{
					{
						Timestamp:   time.Now(),
						Host:        "host",
						Measurement: "measurement",
					},
				},
			},
			true,
		},
		{
			"measurement field names must be non-whitespace",
			fields{
				Db: &mocks.Db{},
			},
			args{
				[]metrics.Measurement{
					{
						Timestamp:   time.Now(),
						Host:        "host",
						Measurement: "measurement",
						Fields: map[string]float64{
							"name": 2,
							"  ":   0,
						},
						Tags: map[string]string{
							"foo": "bar",
						},
					},
				},
			},
			true,
		},
		{
			"measurement field names must be non-whitespace",
			fields{
				Db: &mocks.Db{},
			},
			args{
				[]metrics.Measurement{
					{
						Timestamp:   time.Now(),
						Host:        "host",
						Measurement: "measurement",
						Fields: map[string]float64{
							"name": 2,
						},
						Tags: map[string]string{
							"foo":  "bar",
							"\r\t": "",
						},
					},
				},
			},
			true,
		},
		{
			"transaction begin error is returned",
			fields{
				Db: func() *mocks.Db {
					db := &mocks.Db{}
					db.On("Begin").Return(nil, errors.New("transaction error")).Once()
					return db
				}(),
			},
			args{
				[]metrics.Measurement{
					{
						Timestamp:   time.Now(),
						Host:        "host",
						Measurement: "measurement",
						Fields: map[string]float64{
							"name": 2,
						},
					},
				},
			},
			true,
		},
		{
			"prepared named error is returned",
			func() fields {
				db := &mocks.Db{}
				tx := &mocks.Tx{}
				db.On("Begin").Return(tx, nil).Once()
				tx.On("PrepareNamed", mock.AnythingOfType("string")).Return(nil, errors.New("statement error")).Once()
				return fields{
					db,
					func(t *testing.T) {
						tx.AssertExpectations(t)
					},
				}
			}(),
			args{
				[]metrics.Measurement{
					{
						Timestamp:   time.Now(),
						Host:        "host",
						Measurement: "measurement",
						Fields: map[string]float64{
							"name": 2,
						},
					},
				},
			},
			true,
		},
		{
			"statement exec error causes rollback",
			func() fields {
				db := &mocks.Db{}
				tx := &mocks.Tx{}
				stmt := &mocks.Statement{}
				db.On("Begin").Return(tx, nil).Once()
				tx.On("PrepareNamed", mock.AnythingOfType("string")).Return(stmt, nil).Once()
				stmt.On("Exec", metrics.Model{
					Timestamp:   t0,
					Host:        "host",
					Measurement: "measurement",
					FieldNames:  []string{"name"},
					FieldValues: []float64{2},
					TagKeys:     []string{},
					TagValues:   []string{},
				}).Return(errors.New("exec error")).Once()
				tx.On("Rollback").Return(nil).Once()
				stmt.On("Close").Return(nil).Once()
				return fields{
					db,
					func(t *testing.T) {
						tx.AssertExpectations(t)
						stmt.AssertExpectations(t)
					},
				}
			}(),
			args{
				[]metrics.Measurement{
					{
						Timestamp:   t0,
						Host:        "host",
						Measurement: "measurement",
						Fields: map[string]float64{
							"name": 2,
						},
					},
				},
			},
			true,
		},
		{
			"models are passed to exec with keys sorted",
			func() fields {
				db := &mocks.Db{}
				tx := &mocks.Tx{}
				stmt := &mocks.Statement{}
				db.On("Begin").Return(tx, nil).Once()
				tx.On("PrepareNamed", mock.AnythingOfType("string")).Return(stmt, nil).Once()
				stmt.On("Exec", metrics.Model{
					Timestamp:   t0,
					Host:        "host",
					Measurement: "measurement",
					FieldNames:  []string{"a", "b"},
					FieldValues: []float64{2, 3},
					TagKeys:     []string{},
					TagValues:   []string{},
				}).Return(nil).Once()
				stmt.On("Exec", metrics.Model{
					Timestamp:     t0.Add(time.Second),
					Host:          "host2",
					Measurement:   "measurement2",
					FieldNames:    []string{"c", "d"},
					FieldValues:   []float64{4, 5},
					TagKeys:       []string{"bar", "foo"},
					TagValues:     []string{"bing", "baz"},
					ProjectId:     12,
					EnvironmentId: 1,
				}).Return(nil).Once()
				stmt.On("Close").Return(nil).Once()
				tx.On("Commit").Return(nil).Once()

				return fields{
					db,
					func(t *testing.T) {
						tx.AssertExpectations(t)
						stmt.AssertExpectations(t)
					},
				}
			}(),
			args{
				[]metrics.Measurement{
					{
						Timestamp:   t0,
						Host:        "host",
						Measurement: "measurement",
						Fields: map[string]float64{
							"b": 3,
							"a": 2,
						},
					},
					{
						Timestamp:   t0.Add(time.Second),
						Host:        "host2",
						Measurement: "measurement2",
						Fields: map[string]float64{
							"c": 4,
							"d": 5,
						},
						Tags: map[string]string{
							"foo": "baz",
							"bar": "bing",
						},
						ProjectId:     12,
						EnvironmentId: 1,
					},
				},
			},
			false,
		},
		{
			"transaction commit error is returned",
			func() fields {
				db := &mocks.Db{}
				tx := &mocks.Tx{}
				stmt := &mocks.Statement{}
				db.On("Begin").Return(tx, nil).Once()
				tx.On("PrepareNamed", mock.AnythingOfType("string")).Return(stmt, nil).Once()
				stmt.On("Exec", metrics.Model{
					Timestamp:   t0,
					Host:        "host",
					Measurement: "measurement",
					FieldNames:  []string{"a", "b"},
					FieldValues: []float64{2, 3},
					TagKeys:     []string{},
					TagValues:   []string{},
				}).Return(nil).Once()
				stmt.On("Close").Return(nil).Once()
				tx.On("Commit").Return(errors.New("commit error")).Once()

				return fields{
					db,
					func(t *testing.T) {
						tx.AssertExpectations(t)
						stmt.AssertExpectations(t)
					},
				}
			}(),
			args{
				[]metrics.Measurement{
					{
						Timestamp:   t0,
						Host:        "host",
						Measurement: "measurement",
						Fields: map[string]float64{
							"b": 3,
							"a": 2,
						},
					},
				},
			},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := metrics.NewStore(tt.fields.Db)
			if err := s.Save(tt.args.measurements); (err != nil) != tt.wantErr {
				t.Errorf("store.Save() error = %v, wantErr %v", err, tt.wantErr)
			}
			tt.fields.Db.AssertExpectations(t)
			if tt.fields.assertFun != nil {
				tt.fields.assertFun(t)
			}
		})
	}
}
