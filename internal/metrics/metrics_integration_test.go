// +build integration

package metrics_test

import (
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/metrics"
)

func Test_store_Save_Integration(t *testing.T) {
	db := getDBFromEnv(t)
	defer db.Close()
	store := metrics.NewStore(db)

	t0 := time.Now().UTC()

	hostname := "host-" + t0.Format("20060102150405")

	measurements := make([]metrics.Measurement, 5)
	for i := 0; i < 5; i++ {
		fields := make(map[string]float64, i+1)
		tags := make(map[string]string, i)
		fields[fmt.Sprintf("test-field-%d", 0)] = 0
		for j := 0; j < i; j++ {
			fields[fmt.Sprintf("test-field-%d", j+1)] = float64(j + 1)
			tags[fmt.Sprintf("test-tag-%d", j)] = fmt.Sprintf("test-value-%d", j)
		}

		measurements[i] = metrics.Measurement{
			Timestamp:     t0.Add(time.Second * time.Duration(i)).Truncate(time.Second),
			Host:          hostname,
			Measurement:   "test-measurement",
			ProjectId:     12,
			EnvironmentId: 1,
			Fields:        fields,
			Tags:          tags,
		}
	}

	if err := store.Save(measurements); err != nil {
		t.Fatal(err)
	}

	ms, err := store.Query(12, 1, hostname, "test-measurement", t0, t0.Add(time.Second*time.Duration(5)))
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, measurements, ms)
}

func getDBFromEnv(t *testing.T) metrics.Db {
	testDSN := os.Getenv("TEST_CLICKHOUSE_DSN")
	if testDSN == "" {
		t.Fatal("TEST_CLICKHOUSE_DSN env var must be set")
	}

	db, err := metrics.NewDb(testDSN)
	if err != nil {
		t.Fatalf("new db with test dsn: %v", err)
	}
	return db
}
