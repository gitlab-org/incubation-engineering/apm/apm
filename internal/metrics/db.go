package metrics

import (
	"fmt"

	_ "github.com/ClickHouse/clickhouse-go"
	"github.com/jmoiron/sqlx"
)

// Db interface for testing purposes
type Db interface {
	Queryer
	// Begin begins a new transaction
	Begin() (Tx, error)
	// Close closes any open DB connections
	Close() error
	// Ping verifies database connection
	Ping() error
}

// Queryer implements query specific functions
type Queryer interface {
	// Exec executes a query
	Exec(query string, args ...interface{}) error
	// PrepareNamed returns a named statement
	PrepareNamed(query string) (Statement, error)
	// NamedSelect runs a named query with the specified fields (struct or map).
	// Destination should be a slice of structs.
	NamedSelect(dest interface{}, query string, fields interface{}) error
}

// Tx interface for testing transactions
type Tx interface {
	Queryer
	// Commit commits a transaction
	Commit() error
	// Rollback aborts the transaction
	Rollback() error
}

// Statement representing named statement
type Statement interface {
	// Close closes the statement
	Close() error
	// Exec executes a named statement using the struct passed in
	Exec(arg interface{}) error
}

func NewDb(dsn string) (Db, error) {
	conn, err := sqlx.Open("clickhouse", dsn)
	if err != nil {
		return nil, fmt.Errorf("open dsn: %w", err)
	}
	return &db{
		conn,
	}, nil
}

type db struct {
	*sqlx.DB
}

type tx struct {
	*sqlx.Tx
}

type stmt struct {
	*sqlx.NamedStmt
}

func (d *db) Begin() (Tx, error) {
	t, err := d.Beginx()
	if err != nil {
		return nil, fmt.Errorf("being tx: %w", err)
	}
	return &tx{
		t,
	}, nil
}

func (d *db) PrepareNamed(query string) (Statement, error) {
	s, err := d.DB.PrepareNamed(query)
	if err != nil {
		return nil, fmt.Errorf("prepare named: %w", err)
	}
	return &stmt{
		s,
	}, nil
}

func (d *db) Close() error {
	if err := d.DB.Close(); err != nil {
		return fmt.Errorf("db close: %w", err)
	}
	return nil
}

func (d *db) Ping() error {
	if err := d.DB.Ping(); err != nil {
		return fmt.Errorf("db ping: %w", err)
	}
	return nil
}

func (d *db) Exec(query string, args ...interface{}) error {
	_, err := d.DB.Exec(query, args...)
	if err != nil {
		return fmt.Errorf("exec: %w", err)
	}
	return nil
}

func (d *db) NamedSelect(dest interface{}, query string, fields interface{}) error {
	rows, err := d.DB.NamedQuery(query, fields)
	if err != nil {
		return fmt.Errorf("named query: %w", err)
	}
	defer rows.Close()
	if err := sqlx.StructScan(rows, dest); err != nil {
		return fmt.Errorf("db struct scan: %w", err)
	}
	return nil
}

func (t *tx) Commit() error {
	if err := t.Tx.Commit(); err != nil {
		return fmt.Errorf("tx commit: %w", err)
	}
	return nil
}

func (t *tx) Rollback() error {
	if err := t.Tx.Rollback(); err != nil {
		return fmt.Errorf("tx rollback: %w", err)
	}
	return nil
}

func (t *tx) PrepareNamed(query string) (Statement, error) {
	s, err := t.Tx.PrepareNamed(query)
	if err != nil {
		return nil, fmt.Errorf("tx prepare named: %w", err)
	}
	return &stmt{
		s,
	}, nil
}

func (t *tx) Exec(query string, args ...interface{}) error {
	_, err := t.Tx.Exec(query, args...)
	if err != nil {
		return fmt.Errorf("tx exec: %w", err)
	}
	return nil
}

func (t *tx) NamedSelect(dest interface{}, query string, fields interface{}) error {
	rows, err := t.Tx.NamedQuery(query, fields)
	if err != nil {
		return fmt.Errorf("tx named query: %w", err)
	}
	defer rows.Close()
	if err := sqlx.StructScan(rows, dest); err != nil {
		return fmt.Errorf("tx struct scan: %w", err)
	}
	return nil
}

func (s *stmt) Close() error {
	if err := s.NamedStmt.Close(); err != nil {
		return fmt.Errorf("statement close: %w", err)
	}
	return nil
}

func (s *stmt) Exec(arg interface{}) error {
	_, err := s.NamedStmt.Exec(arg)
	if err != nil {
		return fmt.Errorf("stmt exec: %w", err)
	}
	return nil
}
