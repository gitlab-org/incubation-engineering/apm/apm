package metrics

import (
	"errors"
	"fmt"
	"sort"
	"strings"
	"time"
)

// Measurement of a particular metric group
type Measurement struct {
	// Timestamp for metric in UTC
	Timestamp time.Time
	// Host related to this metric
	Host string
	// Measurement name
	Measurement string
	// ProjectId associated with the measurement
	ProjectId int64
	// EnvironmentId associated with the measurement
	EnvironmentId int64
	// Field values for this measurement
	Fields map[string]float64
	// Tags associated with this measurement
	Tags map[string]string
}

// Store for metrics
type Store interface {
	// Save saves a series of measurements to the data store
	Save([]Measurement) error
	// Query for measurements for a host, measurement and within a time range
	Query(projectId, environmentId int64, host string, measurement string, from, to time.Time) ([]Measurement, error)
}

// NewStore returns a new store implementation for given clickhouse DB
func NewStore(db Db) Store {
	return &store{
		db,
	}
}

type store struct {
	Db
}

type Model struct {
	Timestamp     time.Time
	Host          string
	Measurement   string
	ProjectId     int64     `db:"project_id"`
	EnvironmentId int64     `db:"environment_id"`
	FieldNames    []string  `db:"fields.name"`
	FieldValues   []float64 `db:"fields.value"`
	TagKeys       []string  `db:"tags.key"`
	TagValues     []string  `db:"tags.value"`
}

func (s *store) Save(measurements []Measurement) error {
	if len(measurements) == 0 {
		return nil
	}
	models := make([]Model, len(measurements))
	for i, m := range measurements {
		model, err := m.toModel()
		if err != nil {
			return fmt.Errorf("measurement at %d invalid: %w", i, err)
		}
		models[i] = *model
	}

	tx, err := s.Begin()
	if err != nil {
		return fmt.Errorf("begin transaction: %w", err)
	}
	stmt, err := tx.PrepareNamed(`INSERT INTO metrics (
		timestamp, host, measurement, project_id, environment_id, fields.name, fields.value, tags.key, tags.value
	) VALUES (
		:timestamp, :host, :measurement, :project_id, :environment_id :fields.name, :fields.value, :tags.key, :tags.value
	))`)
	if err != nil {
		return fmt.Errorf("prepare named: %w", err)
	}
	defer stmt.Close()

	for _, m := range models {
		if err := stmt.Exec(m); err != nil {
			_ = tx.Rollback()
			return fmt.Errorf("statement exec: %w", err)
		}
	}

	if err := tx.Commit(); err != nil {
		return fmt.Errorf("transaction commit: %w", err)
	}

	return nil
}

func (s *store) Query(projectId, environmentId int64, host string, measurement string, from, to time.Time) ([]Measurement, error) {
	ms := []Model{}

	err := s.Db.NamedSelect(&ms, `
		SELECT timestamp, host, measurement, project_id, environment_id, fields.name, fields.value, tags.key, tags.value
		FROM metrics
		WHERE project_id = :projectid AND environment_id = :environmentid AND host = :host AND measurement = :measurement AND timestamp BETWEEN :from AND :to
		ORDER BY timestamp, host, measurement
		`,
		struct {
			ProjectId     int64
			EnvironmentId int64
			Host          string
			Measurement   string
			From          time.Time
			To            time.Time
		}{
			projectId,
			environmentId,
			host,
			measurement,
			from,
			to,
		})
	if err != nil {
		return nil, fmt.Errorf("named select: %w", err)
	}

	measurements := make([]Measurement, len(ms))
	for i, m := range ms {
		fields := make(map[string]float64, len(m.FieldNames))
		for i, f := range m.FieldNames {
			fields[f] = m.FieldValues[i]
		}
		tags := make(map[string]string, len(m.TagKeys))
		for i, t := range m.TagKeys {
			tags[t] = m.TagValues[i]
		}
		measurements[i] = Measurement{
			Timestamp:     m.Timestamp,
			Host:          m.Host,
			Measurement:   m.Measurement,
			ProjectId:     m.ProjectId,
			EnvironmentId: m.EnvironmentId,
			Fields:        fields,
			Tags:          tags,
		}
	}
	return measurements, nil
}

func (m Measurement) toModel() (*Model, error) {
	if m.Timestamp.IsZero() {
		return nil, errors.New("timestamp cannot be zero")
	}
	host := strings.TrimSpace(m.Host)
	if host == "" {
		return nil, errors.New("host cannot be empty or whitespace")
	}
	measurement := strings.TrimSpace(m.Measurement)
	if measurement == "" {
		return nil, errors.New("measurement cannot be empty or whitespace")
	}
	if len(m.Fields) == 0 {
		return nil, errors.New("fields cannot be empty")
	}

	fieldKeys := make([]string, len(m.Fields))
	i := 0
	for k := range m.Fields {
		field := strings.TrimSpace(k)
		if field == "" {
			return nil, errors.New("field keys cannot be empty or whitespace")
		}
		fieldKeys[i] = field
		i++
	}
	sort.Strings(fieldKeys)
	fieldValues := make([]float64, len(fieldKeys))
	for i, n := range fieldKeys {
		fieldValues[i] = m.Fields[n]
	}

	tagKeys := make([]string, len(m.Tags))
	i = 0
	for k := range m.Tags {
		tag := strings.TrimSpace(k)
		if tag == "" {
			return nil, errors.New("tag keys cannot be empty or whitespace")
		}
		tagKeys[i] = tag
		i++
	}
	sort.Strings(tagKeys)
	tagValues := make([]string, len(tagKeys))
	for i, n := range tagKeys {
		v := strings.TrimSpace(m.Tags[n])
		tagValues[i] = v
	}

	return &Model{
		Timestamp:     m.Timestamp,
		Host:          host,
		Measurement:   measurement,
		ProjectId:     m.ProjectId,
		EnvironmentId: m.EnvironmentId,
		FieldNames:    fieldKeys,
		FieldValues:   fieldValues,
		TagKeys:       tagKeys,
		TagValues:     tagValues,
	}, nil
}
