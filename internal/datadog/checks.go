package datadog

import "net/http"

// ChecksHandler handles the DataDog check_run endpoint.
// Currently this is a stub to simply suppress agent errors.
func (h *apiHandler) ChecksHandler(w http.ResponseWriter, r *http.Request) {
	httpAccepted(w)
}
