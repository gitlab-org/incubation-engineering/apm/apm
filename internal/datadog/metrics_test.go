package datadog_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/auth"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/datadog"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/metrics"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/mocks"
)

func TestApiHandler_MetricsHandler(t *testing.T) {
	t0 := time.Now().UTC().Truncate(time.Second)
	validPost := func(json string) *http.Request {
		r := httptest.NewRequest("POST", "/api/v1/series", bytes.NewBufferString(json))
		r.Header.Set("DD-API-KEY", "123456")
		return r
	}
	type fields struct {
		Store *mocks.Store
		Auth  *mocks.Auth
	}
	type args struct {
		r *http.Request
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		wantCode int
		wantJSON string
	}{
		{
			"unknown route returns 404",
			fields{
				&mocks.Store{},
				&mocks.Auth{},
			},
			args{
				httptest.NewRequest("POST", "/api/v7/series", nil),
			},
			404,
			"",
		},
		{
			"GET series returns 405",
			fields{
				&mocks.Store{},
				&mocks.Auth{},
			},
			args{
				httptest.NewRequest("GET", "/api/v1/series", nil),
			},
			405,
			"",
		},
		{
			"Broken JSON results in 400",
			fields{
				&mocks.Store{},
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("123456")).Return(&auth.Session{}, nil).Once()
					return m
				}(),
			},
			args{
				validPost("{"),
			},
			400,
			`{"errors":["decoding JSON: unexpected EOF"]}`,
		},
		{
			"Empty JSON object results in 400 and Series required error",
			fields{
				&mocks.Store{},
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("123456")).Return(&auth.Session{}, nil).Once()
					return m
				}(),
			},
			args{
				validPost("{}"),
			},
			400,
			`{"errors":["Series is a required field"]}`,
		},
		{
			"Empty series array results in 400 and required error",
			fields{
				&mocks.Store{},
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("123456")).Return(&auth.Session{}, nil).Once()
					return m
				}(),
			},
			args{
				validPost(`{"series":[]}`),
			},
			400,
			`{"errors":["Series must contain more than 0 items"]}`,
		},
		{
			"Empty series item fields required 400",
			fields{
				&mocks.Store{},
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("123456")).Return(&auth.Session{}, nil).Once()
					return m
				}(),
			},
			args{
				validPost(`{"series":[{}]}`),
			},
			400,
			`{"errors":[
				"Host is a required field",
				"Metric is a required field",
				"Points is a required field"
			]}`,
		},
		{
			"Metrics name must not end with underscore, returns 400",
			fields{
				&mocks.Store{},
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("123456")).Return(&auth.Session{}, nil).Once()
					return m
				}(),
			},
			args{
				validPost(`{"series":[
					{
						"host": "foo",
						"metric": "and_",
						"points": [[1, 0]]
					}
				]}`),
			},
			400,
			`{"errors":[
				"Metric must not end with _"
			]}`,
		},
		{
			"Metrics name cannot start with a period, returns 400",
			fields{
				&mocks.Store{},
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("123456")).Return(&auth.Session{}, nil).Once()
					return m
				}(),
			},
			args{
				validPost(`{"series":[
					{
						"host": "foo",
						"metric": ".bar",
						"points": [[1, 0]]
					}
				]}`),
			},
			400,
			`{"errors":[
				"Metric must not start with ."
			]}`,
		},
		{
			"Empty tag is not allowed, cannot start with colon, returns 400",
			fields{
				&mocks.Store{},
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("123456")).Return(&auth.Session{}, nil).Once()
					return m
				}(),
			},
			args{
				validPost(`{"series":[
					{
						"host": "foo",
						"metric": "bar",
						"points": [[1, 0]],
						"tags": ["", ":tag"]
					}
				]}`),
			},
			400,
			`{"errors":[
				"Tags[0] is a required field",
				"Tags[1] must not start with :"
			]}`,
		},
		{
			"Times must be -1hr to +10min range, returns 400",
			fields{
				&mocks.Store{},
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("123456")).Return(&auth.Session{}, nil).Once()
					return m
				}(),
			},
			args{
				validPost(fmt.Sprintf(`{"series":[
					{
						"host": "foo",
						"metric": "bar",
						"points": [[%d, 0],[%d, 0]]
					}
				]}`, t0.Add(-time.Hour-time.Second).Unix(), t0.Add(time.Minute*11).Unix())),
			},
			400,
			`{"errors":[
				"Series[0].Points[0] time is more than 1 hour old",
				"Series[0].Points[1] time is more than 10 minutes into future"
			]}`,
		},
		{
			"Store error is handled returns 500",
			fields{
				func() *mocks.Store {
					m := &mocks.Store{}
					m.On("Save", []metrics.Measurement{
						{
							Timestamp:   t0,
							Host:        "host1",
							Measurement: "measurement1",
							Fields: map[string]float64{
								"value": 32,
							},
							Tags:          map[string]string{},
							ProjectId:     12,
							EnvironmentId: 1,
						},
					}).Once().Return(errors.New("Save err"))
					return m
				}(),
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("123456")).Return(&auth.Session{
						ProjectId:     12,
						EnvironmentId: 1,
					}, nil).Once()
					return m
				}(),
			},
			args{
				validPost(fmt.Sprintf(`{"series":[
					{
						"host": "host1",
						"metric": "measurement1",
						"points": [[%d, 32]]
					}
				]}`, t0.Unix())),
			},
			500,
			`{"errors":["Internal server error"]}`,
		},
		{
			"Store multiple measurements with and without tags, returns 202",
			fields{
				func() *mocks.Store {
					m := &mocks.Store{}
					m.On("Save", []metrics.Measurement{
						{
							Timestamp:   t0,
							Host:        "host1",
							Measurement: "measurement1",
							Fields: map[string]float64{
								"value": 32,
							},
							Tags:          map[string]string{},
							ProjectId:     5,
							EnvironmentId: 1,
						},
						{
							Timestamp:   t0,
							Host:        "host1",
							Measurement: "measurement.2",
							Fields: map[string]float64{
								"thing":     1.25,
								"neg_thing": -1.25,
							},
							Tags: map[string]string{
								"foo": "bar",
								"baz": "",
							},
							ProjectId:     5,
							EnvironmentId: 1,
						},
						{
							Timestamp:   t0.Add(time.Second),
							Host:        "host1",
							Measurement: "measurement.2",
							Fields: map[string]float64{
								"thing": 1.35,
							},
							Tags: map[string]string{
								"foo": "bar",
								"baz": "",
							},
							ProjectId:     5,
							EnvironmentId: 1,
						},
						{
							Timestamp:   t0,
							Host:        "host2",
							Measurement: "device",
							Fields: map[string]float64{
								"thing": 0.1,
							},
							Tags: map[string]string{
								"device": "first",
							},
							ProjectId:     5,
							EnvironmentId: 1,
						},
						{
							Timestamp:   t0,
							Host:        "host2",
							Measurement: "device",
							Fields: map[string]float64{
								"thing": 0,
							},
							Tags: map[string]string{
								"device": "second",
							},
							ProjectId:     5,
							EnvironmentId: 1,
						},
						{
							Timestamp:   t0,
							Host:        "host2",
							Measurement: "emptytag",
							Fields: map[string]float64{
								"value": 5,
							},
							Tags: map[string]string{
								"thing": "",
							},
							ProjectId:     5,
							EnvironmentId: 1,
						},
					}).Once().Return(nil)
					return m
				}(),
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("123456")).Return(&auth.Session{
						ProjectId:     5,
						EnvironmentId: 1,
					}, nil).Once()
					return m
				}(),
			},
			args{
				validPost(fmt.Sprintf(`{"series":[
					{
						"host": "host1",
						"metric": "measurement1",
						"points": [[%[1]d, 32]]
					},
					{
						"host": "host1",
						"metric": "measurement.2.thing",
						"points": [[%[1]d, 1.25], [%[2]d, 1.35]],
						"tags": ["foo:bar", "baz:"]
					},
					{
						"host": "host1",
						"metric": "measurement.2.neg_thing",
						"points": [[%[1]d, -1.25]],
						"tags": ["foo:bar", "baz:"]
					},
					{
						"host": "host2",
						"metric": "device_thing",
						"points": [[%[1]d, 0.1]],
						"tags": ["device:first"]
					},
					{
						"host": "host2",
						"metric": "device_thing",
						"points": [[%[1]d, 0]],
						"tags": ["device:second"]
					},
					{
						"host": "host2",
						"metric": "emptytag",
						"points": [[%[1]d, 5]],
						"tags": ["thing"]
					}
				]}`, t0.Unix(), t0.Add(time.Second).Unix())),
			},
			202,
			`{"status":"ok"}`,
		},
		{
			"api key is required, returns 403",
			fields{
				&mocks.Store{},
				&mocks.Auth{},
			},
			args{
				httptest.NewRequest("POST", "/api/v1/series", nil),
			},
			403,
			`{"errors":["API key is required"]}`,
		},
		{
			"verify api key forbidden error returns 403",
			fields{
				&mocks.Store{},
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("123456")).Return(nil, auth.ErrForbidden).Once()
					return m
				}(),
			},
			args{
				validPost(fmt.Sprintf(`{"series":[
					{
						"host": "host1",
						"metric": "measurement1",
						"points": [[%d, 32]]
					}]}`, t0.Unix())),
			},
			403,
			`{"errors":["Forbidden"]}`,
		},
		{
			"series points incorrect dimensions returns 400",
			fields{
				&mocks.Store{},
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("123456")).Return(&auth.Session{}, nil).Once()
					return m
				}(),
			},
			args{
				validPost(fmt.Sprintf(`{"series":[
					{
						"host": "host1",
						"metric": "measurement1",
						"points": [[%d, 32, -1]]
					}]}`, t0.Unix())),
			},
			400,
			`{"errors":["decoding JSON: wrong number of fields in series point, expected array of size 2"]}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rr := httptest.NewRecorder()
			r := mux.NewRouter()
			datadog.ApiHandlers(r, tt.fields.Store, tt.fields.Auth)
			r.ServeHTTP(rr, tt.args.r)

			assert.Equal(t, tt.wantCode, rr.Code, "http status code")

			if tt.wantJSON != "" {
				assert.Equal(t, getJSON(t, tt.wantJSON), getJSON(t, rr.Body.String()))
			}

			tt.fields.Store.AssertExpectations(t)
			tt.fields.Auth.AssertExpectations(t)
		})
	}
}

func getJSON(t *testing.T, blob string) interface{} {
	m := map[string]interface{}{}
	if err := json.Unmarshal([]byte(blob), &m); err != nil {
		t.Errorf("Decoding json: %v", err)
	}
	return m
}
