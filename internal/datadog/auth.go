package datadog

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/auth"
)

const (
	contextSession sessionKey = "SESSION"
)

type sessionKey string

// Auth interface to mock auth.Auth struct
type Auth interface {
	// Verify checks an APIKey for valid session or creates one if the key is valid.
	// Returns ErrUnauthorized if the token is not valid.
	// Returns ErrForbidden if the token is not valid for the project/env or is not Developer level access.
	// Returns ErrNotFound if the project/env does not exist.
	// Returns ErrKeyInvalid if the api key format is invalid.
	// Returns unspecified error in the case of unexpected error.
	Verify(key auth.APIKey) (*auth.Session, error)
}

// AuthHandler handles the DataDog validate endpoint for token validation.
// Middleware should verify token before this endpoint is hit.
// Returns 200 with value:true body by default assuming all checks have passed.
func (h *apiHandler) AuthHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, `{"valid": true}`)
}

func (h *apiHandler) verifyAPIKeyMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		key := apiKeyFromRequest(r)
		if key == "" {
			httpForbidden(w, "API key is required")
			return
		}

		session, err := h.auth.Verify(key)

		if err != nil {
			if errors.Is(err, auth.ErrKeyInvalid) {
				httpBadRequest(w, err.Error())
				return
			}
			if errors.Is(err, auth.ErrForbidden) {
				httpForbidden(w, "Forbidden")
				return
			}
			if errors.Is(err, auth.ErrNotFound) {
				httpBadRequest(w, "Project or environment not found")
				return
			}
			if errors.Is(err, auth.ErrUnauthorized) {
				httpForbidden(w, "GitLab token does not have required access")
				return
			}

			httpInternalServerError(w, err)
			return
		}

		ctx := context.WithValue(r.Context(), contextSession, session)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (h *apiHandler) contextSession(r *http.Request) *auth.Session {
	return r.Context().Value(contextSession).(*auth.Session)
}

// API key may come via header or query param
func apiKeyFromRequest(r *http.Request) auth.APIKey {
	apiKey := r.Header.Get("DD-API-KEY")
	if apiKey == "" {
		apiKey = r.URL.Query().Get("api_key")
	}

	return auth.APIKey(apiKey)
}
