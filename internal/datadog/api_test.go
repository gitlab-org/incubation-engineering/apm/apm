package datadog_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/auth"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/datadog"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/mocks"
)

func TestApiHandlers(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name        string
		args        args
		wantURL     string
		wantMethods []string
	}{
		{
			"datadog series route exists",
			args{
				"datadog series",
			},
			"/api/v1/series",
			[]string{"POST"},
		},
		{
			"datadog validate route exists",
			args{
				"datadog auth",
			},
			"/api/v1/validate",
			[]string{"GET"},
		},
		{
			"datadog intake route exists",
			args{
				"datadog intake",
			},
			"/intake/",
			[]string{"POST"},
		},
		{
			"datadog service check route exists",
			args{
				"datadog checks",
			},
			"/api/v1/check_run",
			[]string{"POST"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := mux.NewRouter()
			datadog.ApiHandlers(r, nil, nil)

			named := r.Get(tt.args.name)
			assert.NotNil(t, named)

			url, err := named.URL()
			assert.NoError(t, err)
			assert.Equal(t, tt.wantURL, url.Path)

			methods, err := named.GetMethods()
			assert.NoError(t, err)
			assert.Equal(t, tt.wantMethods, methods)
		})
	}
}

func Test_apiHandler_IntakeHandler(t *testing.T) {
	type fields struct {
		auth *mocks.Auth
	}
	type args struct {
		r *http.Request
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		wantCode int
		wantJSON string
	}{
		{
			"forbidden api key is handled by the middleware, returns 403",
			fields{
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("foo")).Return(nil, auth.ErrForbidden).Once()
					return m
				}(),
			},
			args{
				wrapApiKeyRequest(httptest.NewRequest("POST", "/intake/", nil), "foo"),
			},
			403,
			`{"errors":["Forbidden"]}`,
		},
		{
			"accepted api key, returns 202",
			fields{
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("foo")).Return(&auth.Session{}, nil).Once()
					return m
				}(),
			},
			args{
				wrapApiKeyRequest(httptest.NewRequest("POST", "/intake/", nil), "foo"),
			},
			202,
			`{"status":"ok"}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rr := httptest.NewRecorder()
			r := mux.NewRouter()
			datadog.ApiHandlers(r, nil, tt.fields.auth)
			r.ServeHTTP(rr, tt.args.r)

			assert.Equal(t, tt.wantCode, rr.Code, "http status code")

			if tt.wantJSON != "" {
				assert.Equal(t, getJSON(t, tt.wantJSON), getJSON(t, rr.Body.String()))
			}

			tt.fields.auth.AssertExpectations(t)
		})
	}
}
