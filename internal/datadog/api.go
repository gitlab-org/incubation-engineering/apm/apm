package datadog

import (
	"compress/gzip"
	"compress/zlib"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
	"github.com/gorilla/mux"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/metrics"
	"gitlab.com/gitlab-org/labkit/log"
)

// ApiHandlers attaches DataDog routes to the provided router.
func ApiHandlers(r *mux.Router, store metrics.Store, auth Auth) {
	api := &apiHandler{
		store,
		auth,
		validateFun(),
	}

	middelwares := []mux.MiddlewareFunc{
		contentJSONMiddleware,
		api.verifyAPIKeyMiddleware,
		decodingMiddleware,
	}

	// /intake/ URL is not on api/v1 sub-router
	s := r.PathPrefix("/intake").Subrouter()
	s.Use(middelwares...)
	s.HandleFunc("/", api.IntakeHandler).Methods("POST").Name("datadog intake")

	s = r.PathPrefix("/api/v1").Subrouter()
	s.Use(middelwares...)

	s.HandleFunc("/validate", api.AuthHandler).Methods("GET").Name("datadog auth")

	// following endpoints require auth session to exist
	s.HandleFunc("/series", api.MetricsHandler).Methods("POST").Name("datadog series")
	s.HandleFunc("/check_run", api.ChecksHandler).Methods("POST").Name("datadog checks")
}

// IntakeHandler handles undocumented /intake/ requests which contain arbitrary metadata from the agent.
// /intake/ is not documented under the DataDog API docs.
// We just ignore the content of this and return a 202 status.
func (h *apiHandler) IntakeHandler(w http.ResponseWriter, r *http.Request) {
	httpAccepted(w)
}

type apiHandler struct {
	metrics.Store
	auth           Auth
	validateStruct func(v interface{}) []string
}

type jsonErrors struct {
	Errors []string `json:"errors"`
}

func httpAccepted(w http.ResponseWriter) {
	w.WriteHeader(http.StatusAccepted)
	fmt.Fprintln(w, `{"status":"ok"}`)
}

func httpErrors(w http.ResponseWriter, status int, errs ...string) {
	w.WriteHeader(status)
	_ = json.NewEncoder(w).Encode(jsonErrors{errs})
	for _, err := range errs {
		log.WithError(errors.New(err)).Debug("datadog errors response")
	}
}

func httpBadRequest(w http.ResponseWriter, errors ...string) {
	httpErrors(w, http.StatusBadRequest, errors...)
}

func httpForbidden(w http.ResponseWriter, errors ...string) {
	httpErrors(w, http.StatusForbidden, errors...)
}

func httpInternalServerError(w http.ResponseWriter, err error) {
	const msg = "Internal server error"
	log.WithError(err).Error(msg)
	httpErrors(w, http.StatusInternalServerError, msg)
}

func validateFun() func(val interface{}) []string {
	// instance closure for struct caching
	v := validator.New()

	// use en validation translations by default
	en := en.New()
	uni := ut.New(en, en)

	trans, _ := uni.GetTranslator("en")

	_ = en_translations.RegisterDefaultTranslations(v, trans)
	customTranslations(v, trans)

	return func(val interface{}) []string {
		err := v.Struct(val)
		if err == nil {
			return nil
		}

		errs := err.(validator.ValidationErrors)
		if len(errs) > 0 {
			ss := make([]string, len(errs))
			for i, err := range errs {
				ss[i] = err.Translate(trans)
			}
			return ss
		}
		return nil
	}
}

func customTranslations(v *validator.Validate, trans ut.Translator) {
	translations := []struct {
		tag         string
		translation string
		param       bool
	}{
		{
			"startsnotwith",
			"{0} must not start with {1}",
			true,
		},
		{
			"endsnotwith",
			"{0} must not end with {1}",
			true,
		},
		{
			"containsany",
			"{0} can contain any of {1}",
			true,
		},
	}

	for _, t := range translations {
		_ = v.RegisterTranslation(t.tag, trans, registrationFunc(t.tag, t.translation, false), translateFunc(t.param))
	}
}

func registrationFunc(tag string, translation string, override bool) validator.RegisterTranslationsFunc {
	return func(ut ut.Translator) (err error) {
		if err = ut.Add(tag, translation, override); err != nil {
			return
		}

		return
	}
}

func translateFunc(param bool) validator.TranslationFunc {
	return func(ut ut.Translator, fe validator.FieldError) string {
		var t string
		var err error
		if param {
			t, err = ut.T(fe.Tag(), fe.Field(), fe.Param())
		} else {
			t, err = ut.T(fe.Tag(), fe.Field())
		}
		if err != nil {
			return fe.(error).Error()
		}

		return t
	}
}

func decodingMiddleware(next http.Handler) http.Handler {
	const maxBodySize = 3200000
	const maxDecompressedBodySize = 62914560

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// supports gzip or zlib compression of the body
		contentEncoding := r.Header.Get("Content-Encoding")
		if contentEncoding == "" {
			r.Body = http.MaxBytesReader(w, r.Body, maxBodySize)
		} else if contentEncoding == "gzip" {
			d, err := gzip.NewReader(r.Body)
			if err != nil {
				httpBadRequest(w, "Content-Encoding gzip body read error")
				return
			}
			r.Body = http.MaxBytesReader(w, d, maxDecompressedBodySize)
		} else if contentEncoding == "deflate" {
			d, err := zlib.NewReader(r.Body)
			if err != nil {
				httpBadRequest(w, "Content-Encoding deflate body read error")
				return
			}
			r.Body = http.MaxBytesReader(w, d, maxDecompressedBodySize)
		} else {
			httpBadRequest(w, fmt.Sprintf("Unknown Content-Encoding %s", contentEncoding))
			return
		}

		next.ServeHTTP(w, r)
	})
}

func contentJSONMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}
