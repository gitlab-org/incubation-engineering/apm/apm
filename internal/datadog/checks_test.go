package datadog_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/auth"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/datadog"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/mocks"
)

func Test_apiHandler_ChecksHandler(t *testing.T) {
	type fields struct {
		auth *mocks.Auth
	}
	type args struct {
		r *http.Request
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		wantCode int
		wantJSON string
	}{
		{
			"api key header is required, returns 403",
			fields{
				&mocks.Auth{},
			},
			args{
				wrapApiKeyRequest(httptest.NewRequest("POST", "/api/v1/check_run", nil), ""),
			},
			403,
			`{"errors":["API key is required"]}`,
		},
		{
			"returns 202 by default",
			fields{
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("1234")).Return(&auth.Session{}, nil).Once()
					return m
				}(),
			},
			args{
				wrapApiKeyRequest(httptest.NewRequest("POST", "/api/v1/check_run", nil), "1234"),
			},
			202,
			`{"status":"ok"}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rr := httptest.NewRecorder()
			r := mux.NewRouter()
			datadog.ApiHandlers(r, nil, tt.fields.auth)
			r.ServeHTTP(rr, tt.args.r)

			assert.Equal(t, tt.wantCode, rr.Code, "http status code")

			if tt.wantJSON != "" {
				assert.Equal(t, getJSON(t, tt.wantJSON), getJSON(t, rr.Body.String()))
			}

			tt.fields.auth.AssertExpectations(t)
		})
	}
}
