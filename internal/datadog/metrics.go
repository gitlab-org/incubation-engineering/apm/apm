package datadog

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"sort"
	"strings"
	"time"

	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/metrics"
)

// MetricsHandler handles the DataDog series endpoint.
func (h *apiHandler) MetricsHandler(w http.ResponseWriter, r *http.Request) {
	session := h.contextSession(r)
	if session == nil {
		httpInternalServerError(w, errors.New("Request session in context was nil"))
		return
	}

	var series seriesBody
	if err := json.NewDecoder(r.Body).Decode(&series); err != nil {
		httpBadRequest(w, fmt.Sprintf("decoding JSON: %v", err))
		return
	}

	if errs := h.validateStruct(series); len(errs) > 0 {
		httpBadRequest(w, errs...)
		return
	}

	type seriesKey struct {
		timestamp   time.Time
		host        string
		measurement string
		tagset      string
	}
	type seriesValue struct {
		fields map[string]float64
		tags   map[string]string
	}

	// retain group order
	groupKeys := []seriesKey{}
	groups := map[seriesKey]seriesValue{}

	tzero := time.Now().UTC()

	var errs []string

	for i, s := range series.Series {
		measurement, field := splitMetric(s.Metric)

		// sort tags so we can create a unique tagset
		sort.Strings(s.Tags)
		tagset := strings.Join(s.Tags, ",")

		for j, p := range s.Points {
			ts := time.Unix(p.Time, 0).UTC()
			tdiff := tzero.Sub(ts)
			if tdiff < -time.Minute*10 {
				errs = append(errs, fmt.Sprintf("Series[%d].Points[%d] time is more than 10 minutes into future", i, j))
			} else if tdiff > time.Hour {
				errs = append(errs, fmt.Sprintf("Series[%d].Points[%d] time is more than 1 hour old", i, j))
			}
			key := seriesKey{
				timestamp:   ts,
				host:        s.Host,
				measurement: measurement,
				tagset:      tagset,
			}
			val, got := groups[key]
			if !got {
				groupKeys = append(groupKeys, key)
				val.fields = map[string]float64{}
			}
			val.fields[field] = p.Value

			// tagset may already exist, don't bother doing it again
			if val.tags == nil {
				val.tags = s.Tags.AsMap()
			}

			if !got {
				groups[key] = val
			}
		}
	}

	if len(errs) > 0 {
		httpBadRequest(w, errs...)
		return
	}

	pid := int64(session.ProjectId)
	eid := int64(session.EnvironmentId)

	measurements := []metrics.Measurement{}
	// TODO: this probably should be sorted for insert performance
	for _, k := range groupKeys {
		v := groups[k]
		measurements = append(measurements, metrics.Measurement{
			Timestamp:     k.timestamp,
			Host:          k.host,
			Measurement:   k.measurement,
			Fields:        v.fields,
			Tags:          v.tags,
			ProjectId:     pid,
			EnvironmentId: eid,
		})
	}

	if err := h.Store.Save(measurements); err != nil {
		httpInternalServerError(w, err)
		return
	}

	httpAccepted(w)
}

type seriesBody struct {
	Series []seriesItem `validate:"required,gt=0,dive,required"`
}

type seriesItem struct {
	Host     string `validate:"required"`
	Interval int64
	Metric   string        `validate:"required,lte=200,startsnotwith=.,startsnotwith=_,alphanum|containsany=._,endsnotwith=.,endsnotwith=_"`
	Points   []seriesPoint `validate:"required"`
	Tags     Tags          `validate:"dive,required,startsnotwith=:"`
	Type     string
}

type seriesPoint struct {
	Time  int64 `validate:"gt=0"`
	Value float64
}

func (s *seriesPoint) UnmarshalJSON(buf []byte) error {
	tmp := []interface{}{&s.Time, &s.Value}
	if err := json.Unmarshal(buf, &tmp); err != nil {
		return fmt.Errorf("unmarshal series point: %w", err)
	}
	if len(tmp) != 2 {
		return errors.New("wrong number of fields in series point, expected array of size 2")
	}
	return nil
}

func splitMetric(metric string) (measurement, field string) {
	// periods have precedence over underscores
	if lastDot := strings.LastIndex(metric, "."); lastDot > 0 {
		measurement = metric[:lastDot]
		field = metric[lastDot+1:]
	} else if lastUnderscore := strings.LastIndex(metric, "_"); lastUnderscore > 0 {
		measurement = metric[:lastUnderscore]
		field = metric[lastUnderscore+1:]
		// no periods or underscores, field becomes "value"
	} else {
		measurement = metric
		field = "value"
	}

	return
}

// Tags represents tags as sent by DataDog.
// Tags are string lists with colon separated key:value pairs
type Tags []string

func (ts Tags) AsMap() map[string]string {
	tags := make(map[string]string, len(ts))
	for _, t := range ts {
		tk, tv := splitTag(t)
		tags[tk] = tv
	}
	return tags
}

func splitTag(tag string) (key, value string) {
	if firstColon := strings.Index(tag, ":"); firstColon > 0 {
		key = tag[:firstColon]
		value = tag[firstColon+1:]
	} else {
		key = tag
	}

	return
}
