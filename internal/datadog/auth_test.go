package datadog_test

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/auth"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/datadog"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/mocks"
)

func wrapApiKeyRequest(r *http.Request, apiKey string) *http.Request {
	r.Header.Set("DD-API-KEY", apiKey)
	return r
}

func Test_apiHandler_AuthHandler(t *testing.T) {
	authRequest := func(apiKey string) *http.Request {
		return wrapApiKeyRequest(httptest.NewRequest("GET", "/api/v1/validate", nil), apiKey)
	}
	type fields struct {
		auth *mocks.Auth
	}
	type args struct {
		r *http.Request
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		wantCode int
		wantJSON string
	}{
		{
			"api key header is required, returns 403",
			fields{
				&mocks.Auth{},
			},
			args{
				authRequest(""),
			},
			403,
			`{"errors":["API key is required"]}`,
		},
		{
			"api key syntax is invalid, returns 400",
			fields{
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("foo")).Return(nil, auth.ErrKeyInvalid).Once()
					return m
				}(),
			},
			args{
				authRequest("foo"),
			},
			400,
			`{"errors":["API Key is invalid"]}`,
		},
		{
			"key is forbidden, returns 403",
			fields{
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("foo")).Return(nil, auth.ErrForbidden).Once()
					return m
				}(),
			},
			args{
				authRequest("foo"),
			},
			403,
			`{"errors":["Forbidden"]}`,
		},
		{
			"project for key is not found, returns 400",
			fields{
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("foo")).Return(nil, auth.ErrNotFound).Once()
					return m
				}(),
			},
			args{
				authRequest("foo"),
			},
			400,
			`{"errors":["Project or environment not found"]}`,
		},
		{
			"api key is unauthorized, returns 403",
			fields{
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("foo")).Return(nil, auth.ErrUnauthorized).Once()
					return m
				}(),
			},
			args{
				authRequest("foo"),
			},
			403,
			`{"errors":["GitLab token does not have required access"]}`,
		},
		{
			"unknown verify error returns 500",
			fields{
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("foo")).Return(nil, errors.New("unknown")).Once()
					return m
				}(),
			},
			args{
				authRequest("foo"),
			},
			500,
			`{"errors":["Internal server error"]}`,
		},
		{
			"accepted API token returns 200",
			fields{
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("foo")).Return(&auth.Session{}, nil).Once()
					return m
				}(),
			},
			args{
				authRequest("foo"),
			},
			200,
			`{"valid": true}`,
		},
		{
			"valid api key can be in query string, returns 200",
			fields{
				func() *mocks.Auth {
					m := &mocks.Auth{}
					m.On("Verify", auth.APIKey("foo")).Return(&auth.Session{}, nil).Once()
					return m
				}(),
			},
			args{
				httptest.NewRequest("GET", "/api/v1/validate?api_key=foo", nil),
			},
			200,
			`{"valid": true}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rr := httptest.NewRecorder()
			r := mux.NewRouter()
			datadog.ApiHandlers(r, nil, tt.fields.auth)
			r.ServeHTTP(rr, tt.args.r)

			assert.Equal(t, tt.wantCode, rr.Code, "http status code")

			if tt.wantJSON != "" {
				assert.Equal(t, getJSON(t, tt.wantJSON), getJSON(t, rr.Body.String()))
			}

			tt.fields.auth.AssertExpectations(t)
		})
	}
}
