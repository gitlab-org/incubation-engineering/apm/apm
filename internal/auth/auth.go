package auth

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/go-redis/redis/v8"
)

const (
	projectKey     = "project"
	environmentKey = "environment"
	ErrKeyInvalid  = Error("API Key is invalid")
)

// APIKey represents a reusable key for agent usage.
// APIKey format is that of <GitLab API key>:<Project ID>[:Environment ID]
type APIKey string

// Unpack API key into its parts - GitLab API key, project id and optional environment id.
// Error is returned if there are too few or too many parts in the key.
// Error is returned if required parts are empty or whitespace.
func (k APIKey) Unpack() (key, projectId, environmentId string, err error) {
	parts := strings.Split(string(k), ":")
	if len(parts) < 2 {
		err = errors.New("APIKey must have at least two parts seperated by colons")
		return
	}
	if len(parts) > 3 {
		err = errors.New("APIKey must have at most three parts seperated by colons")
		return
	}

	// environment ID can be empty
	if len(parts) == 3 {
		environmentId = strings.TrimSpace(parts[2])
	}

	key = strings.TrimSpace(parts[0])
	if key == "" {
		err = errors.New("APIKey key part cannot be an empty or whitespace")
		return
	}
	projectId = strings.TrimSpace(parts[1])
	if projectId == "" {
		err = errors.New("APIKey project id part cannot be empty or whitespace")
		return
	}
	return
}

// Auth provides validation of keys and projects as well as basic session storage via redis.
type Auth struct {
	// GlAPI is an interface to a GitLab instance to allow validating keys and getting project info
	GlAPI GitLabAPI
	// SessionDuration is the duration of sessions in redis. Zero duration means infinite expiry.
	// Expired sessions are deleted and new sessions will be created.
	SessionDuration time.Duration
	// Cache uses Redis interface used by auth to store and retrieve sessions by id
	Cache Redis
}

// Session represents a valid api key linked to a GitLab project and optionally environment.
type Session struct {
	// Id of the session
	Id string
	// ProjectId is linked GitLab project id
	ProjectId int
	// EnvironmentId is linked GitLab environment id or 0 if not specified
	EnvironmentId int
}

// Redis exposes specific operations we need for the session store.
type Redis interface {
	Pipelined(ctx context.Context, fn func(Redis) error) error
	HMGet(ctx context.Context, key string, fields ...string) *redis.SliceCmd
	HMSet(ctx context.Context, key string, values ...interface{}) *redis.BoolCmd
	Expire(ctx context.Context, key string, expiration time.Duration) *redis.BoolCmd
	Ping(ctx context.Context) *redis.StatusCmd
}

// Verify takes an api key which contains required info to validate against GitLab
// and get project ID and optional environment IDs.
// Returns ErrUnauthorized if the token is not valid.
// Returns ErrForbidden if the token is not valid for the project/env or is not Developer level access.
// Returns ErrNotFound if the project/env does not exist.
// Returns ErrKeyInvalid if the api key format is invalid.
// Returns unspecified error in the case of unexpected error.
func (a *Auth) Verify(key APIKey) (*Session, error) {
	// project/env ids can be int ids or paths
	apiKey, projectId, envId, err := key.Unpack()
	if err != nil {
		return nil, fmt.Errorf("%w: %v", ErrKeyInvalid, err)
	}

	sessionId := Hash(apiKey, projectId, envId)

	// get int proj/env ids
	pid, eid, err := a.GetSession(sessionId)
	if err != nil && errors.Is(err, ErrNotFound) {
		proj, err := a.GetProjectInfo(apiKey, projectId, envId)
		if err != nil {
			return nil, err
		}

		if !proj.Permissions.ProjectAccess.IsAtLeastDeveloper() {
			return nil, ErrForbidden
		}

		if err := a.StoreSession(sessionId, proj); err != nil {
			return nil, err
		}

		pid = proj.ID
		if proj.Environment != nil {
			eid = proj.Environment.ID
		}
	} else if err != nil {
		return nil, err
	}

	return &Session{
		Id:            sessionId,
		ProjectId:     pid,
		EnvironmentId: eid,
	}, nil
}

// Valid returns true if token is valid.
// Returns error if an unknown error occurred checking the token.
// GitLab project access must be at least Developer level.
//
func (a *Auth) Valid(token string) (bool, error) {
	err := a.GlAPI.Validate(token)

	if err != nil {
		if errors.Is(err, ErrForbidden) || errors.Is(err, ErrUnauthorized) {
			return false, nil
		}

		return false, fmt.Errorf("token validate: %w", err)
	}

	return true, nil
}

// GetProjectInfo gets a project by token and project id.
// If the environment id is set this is also requested and populated in the project.
// Returns error if unknown error occurs.
func (a *Auth) GetProjectInfo(token, projectId, environmentId string) (*Project, error) {
	if token == "" {
		return nil, errors.New("token is required")
	}
	if projectId == "" {
		return nil, errors.New("project id is required")
	}

	proj, err := a.GlAPI.GetProject(token, projectId)
	if err != nil {
		return nil, fmt.Errorf("get project %s: %w", projectId, err)
	}

	if environmentId == "" {
		return proj, nil
	}

	envs, err := a.GlAPI.GetEnvironments(token, projectId)
	if err != nil {
		return nil, fmt.Errorf("get project environments %s: %w", projectId, err)
	}

	for _, e := range envs {
		if strconv.Itoa(e.ID) == environmentId || e.Name == environmentId {
			env := e
			proj.Environment = &env
			return proj, nil
		}
	}

	return nil, fmt.Errorf("environment %s for project %s not found: %w", environmentId, projectId, ErrNotFound)
}

// StoreSession stores a session id against project info for later retrieval
func (a *Auth) StoreSession(sessionId string, project *Project) error {
	if sessionId == "" {
		return errors.New("session ID is required")
	}

	ctx := context.TODO()
	err := a.Cache.Pipelined(ctx, func(pl Redis) error {
		var env string
		if project.Environment != nil {
			env = strconv.Itoa(project.Environment.ID)
		}
		proj := strconv.Itoa(project.ID)
		_ = pl.HMSet(ctx, sessionId, projectKey, proj, environmentKey, env)

		if a.SessionDuration == 0 {
			return nil
		}
		_ = pl.Expire(ctx, sessionId, a.SessionDuration)
		return nil
	})

	if err != nil {
		return fmt.Errorf("session %s store: %w", sessionId, err)
	}

	return nil
}

// GetSession gets project/environment IDs by session ID.
// If the session exists the expiry will be reset.
// Returns ErrNotFound if the session id is not in the cache.
// Returns cache related errors if they occur.
func (a *Auth) GetSession(sessionId string) (projectId, environmentId int, err error) {
	if sessionId == "" {
		err = errors.New("session ID is required")
		return
	}

	sc := a.Cache.HMGet(context.TODO(), sessionId, projectKey, environmentKey)

	vs, err := sc.Result()
	if err != nil {
		err = fmt.Errorf("session %s result: %w", sessionId, err)
		return
	}
	if len(vs) == 0 {
		err = ErrNotFound
		return
	}

	if len(vs) != 2 {
		err = fmt.Errorf("session %s results expected %d got %d", sessionId, 2, len(vs))
		return
	}

	// HMGet returns [nil, nil] if neither field exists for this key
	proj := vs[0]
	env := vs[1]
	if proj == nil {
		err = ErrNotFound
		return
	}
	projectId, err = strconv.Atoi(proj.(string))
	if err != nil {
		err = fmt.Errorf("session %s project id is not an int: %w", sessionId, err)
		return
	}

	if env != nil && env.(string) != "" {
		environmentId, err = strconv.Atoi(env.(string))
		if err != nil {
			err = fmt.Errorf("session %s environment id is not an int: %w", sessionId, err)
		}
	}

	return
}

// Hash combines a series of strings into a SHA256 hash value.
// Returns the hex encoded string.
// To avoid collisions each string part is prefixed with its length.
func Hash(part ...string) string {
	in := ""
	for _, p := range part {
		in += fmt.Sprintf("%d%s", len(part), p)
	}
	bs := sha256.Sum256([]byte(in))
	return hex.EncodeToString(bs[:])
}

func NewCache(c redis.Cmdable) Redis {
	return &cache{
		c,
	}
}

type cache struct {
	c redis.Cmdable
}

// Pipelined runs commands inside a pipeline of commands for fewer roundtrips
func (c *cache) Pipelined(ctx context.Context, fn func(Redis) error) error {
	_, err := c.c.Pipelined(ctx, func(p redis.Pipeliner) error {
		return fn(&cache{p})
	})
	if err != nil {
		return fmt.Errorf("pipelined: %w", err)
	}
	return nil
}

func (c *cache) HMGet(ctx context.Context, key string, fields ...string) *redis.SliceCmd {
	return c.c.HMGet(ctx, key, fields...)
}

func (c *cache) HMSet(ctx context.Context, key string, values ...interface{}) *redis.BoolCmd {
	return c.c.HMSet(ctx, key, values...)
}

func (c *cache) Expire(ctx context.Context, key string, expiration time.Duration) *redis.BoolCmd {
	return c.c.Expire(ctx, key, expiration)
}

func (c *cache) Ping(ctx context.Context) *redis.StatusCmd {
	return c.c.Ping(ctx)
}

func (c *cache) Del(ctx context.Context, keys ...string) *redis.IntCmd {
	return c.c.Del(ctx, keys...)
}
