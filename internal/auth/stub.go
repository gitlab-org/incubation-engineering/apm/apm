package auth

import (
	"fmt"
	"net/http"
	"net/url"

	"github.com/gorilla/mux"
)

// GitLabStubHandlers registers handlers for use as a stub GitLab API
// apiKeys used to match PRIVATE-TOKEN headers
// projects and environments map used to return 200 responses JSON
func GitLabStubHandlers(r *mux.Router, apiKeys []string, projects map[string]string, environments map[string]string) {
	sr := r.PathPrefix("/api/v4").Subrouter().UseEncodedPath()

	ks := make(map[string]struct{}, len(apiKeys))
	for _, k := range apiKeys {
		ks[k] = struct{}{}
	}
	h := stubHandler{
		ks,
		projects,
		environments,
	}

	sr.HandleFunc("/projects/{project_id}", h.projectsHandler).Methods("GET")
	sr.HandleFunc("/projects/{project_id}/environments", h.environmentsHandler).Methods("GET")
	sr.HandleFunc("/version", h.versionHandler).Methods("GET")

	sr.Use(h.authMiddleware)
}

type stubHandler struct {
	apiKeys      map[string]struct{}
	projects     map[string]string
	environments map[string]string
}

func (h stubHandler) projectsHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectId, err := url.PathUnescape(vars["project_id"])
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	if project, got := h.projects[projectId]; got {
		// project taken from GitLab API docs
		fmt.Fprintln(w, project)
		return
	}

	http.Error(w, "Not Found", http.StatusNotFound)
}

func (h stubHandler) environmentsHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectId, err := url.PathUnescape(vars["project_id"])
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	if environments, got := h.environments[projectId]; got {
		fmt.Fprint(w, environments)
		return
	}

	http.Error(w, "Not Found", http.StatusNotFound)
}

func (h stubHandler) versionHandler(w http.ResponseWriter, r *http.Request) {
	// dummy version JSON
	fmt.Fprint(w, `{"version":"14.5.0-pre","revision":"f532886ea25"}`)
}

func (h stubHandler) authMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("PRIVATE-TOKEN")

		if _, got := h.apiKeys[token]; !got {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}

		next.ServeHTTP(w, r)
	})
}
