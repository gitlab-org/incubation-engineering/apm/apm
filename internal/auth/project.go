// project package validates a project against GitLab

package auth

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

const (
	ErrUnauthorized = Error("user is not authorized")
	ErrForbidden    = Error("user is forbidden")
	ErrNotFound     = Error("not found")
)

type Error string

func (e Error) Error() string { return string(e) }

// GitLabAPI interface used to access HTTP API
type GitLabAPI interface {
	// GetProject gets a project with ID and apikey
	// Returns project or error as defined in this package
	GetProject(apiKey, projectID string) (*Project, error)
	// GetEnvironments gets project environments with project ID and apikey
	// Returns project or error as defined in this package
	GetEnvironments(apiKey, projectID string) ([]ProjectEnvironment, error)
	// Validate checks an API key against the GitLab instance.
	// Returns error as defined in this package or nil if valid.
	Validate(apiKey string) error
}

// Project info from GitLab
type Project struct {
	// ID of project
	ID int `json:"id"`
	// PathWithNamespace for project, e.g. namespace/name
	PathWithNamespace string `json:"path_with_namespace"`
	// Permissions set if user has some authenticated permissions
	Permissions ProjectPermissions `json:"permissions"`
	// Environment attached to the project
	Environment *ProjectEnvironment `json:"-"`
}

type ProjectPermissions struct {
	ProjectAccess ProjectAccess `json:"project_access"`
}

type ProjectAccess struct {
	AccessLevel int `json:"access_level"`
}

func (p ProjectAccess) IsAtLeastDeveloper() bool {
	return p.AccessLevel >= 30
}

type ProjectEnvironment struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// NewAPI returns a GitLab API client interface
// http client param should have the base URL configured
// baseURL is the base url of the GitLab instance
func NewAPI(c *http.Client, baseURL *url.URL) GitLabAPI {
	return &gitlabAPI{
		c,
		baseURL,
	}
}

type gitlabAPI struct {
	c    *http.Client
	base *url.URL
}

func (g gitlabAPI) GetProject(apiKey, projectID string) (*Project, error) {
	path, err := url.Parse(fmt.Sprintf("/api/v4/projects/%s", url.PathEscape(projectID)))
	if err != nil {
		return nil, fmt.Errorf("projects path: %w", err)
	}

	res, err := g.request(apiKey, g.base.ResolveReference(path).String())
	if err != nil {
		return nil, fmt.Errorf("project request: %w", err)
	}
	body := res.Body
	defer body.Close()

	if res.StatusCode == http.StatusOK {
		proj := &Project{}
		if err := json.NewDecoder(body).Decode(&proj); err != nil {
			return nil, fmt.Errorf("project decode: %w", err)
		}

		return proj, nil
	}

	return nil, g.genericError(res.StatusCode)
}

func (g gitlabAPI) GetEnvironments(apiKey, projectID string) ([]ProjectEnvironment, error) {
	path, err := url.Parse(fmt.Sprintf("/api/v4/projects/%s/environments", url.PathEscape(projectID)))
	if err != nil {
		return nil, fmt.Errorf("environments path: %w", err)
	}

	res, err := g.request(apiKey, g.base.ResolveReference(path).String())
	if err != nil {
		return nil, fmt.Errorf("environments request: %w", err)
	}
	body := res.Body
	defer body.Close()

	if res.StatusCode == http.StatusOK {
		envs := []ProjectEnvironment{}
		if err := json.NewDecoder(body).Decode(&envs); err != nil {
			return nil, fmt.Errorf("environments decode: %w", err)
		}

		return envs, nil
	}

	return nil, g.genericError(res.StatusCode)
}

func (g gitlabAPI) Validate(apiKey string) error {
	path, err := url.Parse("/api/v4/version")
	if err != nil {
		return fmt.Errorf("version path: %w", err)
	}

	res, err := g.request(apiKey, g.base.ResolveReference(path).String())
	if err != nil {
		return fmt.Errorf("version request: %w", err)
	}
	body := res.Body
	defer body.Close()

	if res.StatusCode == http.StatusOK {
		return nil
	}

	return g.genericError(res.StatusCode)
}

func (g gitlabAPI) request(apiKey, resource string) (*http.Response, error) {
	req, err := http.NewRequest("GET", resource, nil)
	if err != nil {
		return nil, fmt.Errorf("new request: %w", err)
	}
	req.Header.Set("PRIVATE-TOKEN", apiKey)
	r, err := g.c.Do(req)
	if err != nil {
		return nil, fmt.Errorf("http request %w", err)
	}
	return r, nil
}

func (g gitlabAPI) genericError(status int) error {
	switch status {
	case http.StatusNotFound:
		return ErrNotFound
	case http.StatusUnauthorized:
		return ErrUnauthorized
	case http.StatusForbidden:
		return ErrForbidden
	}

	return fmt.Errorf("http status unhandled %d", status)
}
