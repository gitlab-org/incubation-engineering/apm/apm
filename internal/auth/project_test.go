// project package validates a project against GitLab

package auth_test

import (
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"reflect"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/auth"
)

func Test_gitlabAPI_GetProject(t *testing.T) {
	r := mux.NewRouter()
	r.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.Println(r.RequestURI)
			next.ServeHTTP(w, r)
		})
	})
	auth.GitLabStubHandlers(r,
		[]string{"123456"},
		map[string]string{
			"1": `{
				"id": 1,
				"path_with_namespace": "namespace/example",
				"permissions": {
					"project_access": {
						"access_level": 30
					}
				}
			}`,
			"namespace/example": `{
				"id": 1,
				"path_with_namespace": "namespace/example"
			}`,
			"broken": "{",
		},
		map[string]string{})
	ts := httptest.NewServer(r)
	defer ts.Close()
	type args struct {
		apiKey    string
		projectID string
	}
	tests := []struct {
		name     string
		args     args
		want     *auth.Project
		wantErr  bool
		wantErrT error
	}{
		{
			"api key is not authorized",
			args{
				apiKey:    "foo",
				projectID: "1",
			},
			nil,
			true,
			auth.ErrUnauthorized,
		},
		{
			"project not found",
			args{
				apiKey:    "123456",
				projectID: "unknown",
			},
			nil,
			true,
			auth.ErrNotFound,
		},
		{
			"broken JSON error is handled",
			args{
				apiKey:    "123456",
				projectID: "broken",
			},
			nil,
			true,
			nil,
		},
		{
			"project with integer id is decoded",
			args{
				apiKey:    "123456",
				projectID: "1",
			},
			&auth.Project{
				ID:                1,
				PathWithNamespace: "namespace/example",
				Permissions: auth.ProjectPermissions{
					ProjectAccess: auth.ProjectAccess{
						AccessLevel: 30,
					},
				},
			},
			false,
			nil,
		},
		{
			"project with namespace path is decoded",
			args{
				apiKey:    "123456",
				projectID: "namespace/example",
			},
			&auth.Project{
				ID:                1,
				PathWithNamespace: "namespace/example",
			},
			false,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			url, err := url.Parse(ts.URL)
			if err != nil {
				t.Fatal(err)
			}
			g := auth.NewAPI(&http.Client{}, url)
			got, err := g.GetProject(tt.args.apiKey, tt.args.projectID)
			if (err != nil) != tt.wantErr {
				t.Errorf("gitlabAPI.GetProject() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("gitlabAPI.GetProject() = %v, want %v", got, tt.want)
			}
			if tt.wantErrT != nil {
				assert.ErrorIs(t, err, tt.wantErrT)
			}
		})
	}
}

func Test_gitlabAPI_GetEnvironments(t *testing.T) {
	r := mux.NewRouter()
	r.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.Println(r.RequestURI)
			next.ServeHTTP(w, r)
		})
	})
	auth.GitLabStubHandlers(r,
		[]string{"123456"},
		map[string]string{},
		map[string]string{
			"1": `[
				{
					"id": 1,
					"name": "foo"
				}
			]`,
			"namespace/example": `[
				{
					"id": 1,
					"name": "foo"
				}
			]`,
			"broken": "[",
		})
	ts := httptest.NewServer(r)
	defer ts.Close()
	type args struct {
		apiKey    string
		projectID string
	}
	tests := []struct {
		name     string
		args     args
		want     []auth.ProjectEnvironment
		wantErr  bool
		wantErrT error
	}{
		{
			"api key is not authorized",
			args{
				apiKey:    "foo",
				projectID: "1",
			},
			nil,
			true,
			auth.ErrUnauthorized,
		},
		{
			"project not found",
			args{
				apiKey:    "123456",
				projectID: "unknown",
			},
			nil,
			true,
			auth.ErrNotFound,
		},
		{
			"broken JSON error is handled",
			args{
				apiKey:    "123456",
				projectID: "broken",
			},
			nil,
			true,
			nil,
		},
		{
			"environments with integer id is decoded",
			args{
				apiKey:    "123456",
				projectID: "1",
			},
			[]auth.ProjectEnvironment{
				{
					ID:   1,
					Name: "foo",
				},
			},
			false,
			nil,
		},
		{
			"environments with namespace path is decoded",
			args{
				apiKey:    "123456",
				projectID: "namespace/example",
			},
			[]auth.ProjectEnvironment{
				{
					ID:   1,
					Name: "foo",
				},
			},
			false,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			url, err := url.Parse(ts.URL)
			if err != nil {
				t.Fatal(err)
			}
			g := auth.NewAPI(&http.Client{}, url)
			got, err := g.GetEnvironments(tt.args.apiKey, tt.args.projectID)
			if (err != nil) != tt.wantErr {
				t.Errorf("gitlabAPI.GetEnvironments() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("gitlabAPI.GetEnvironments() = %v, want %v", got, tt.want)
			}
			if tt.wantErrT != nil {
				assert.ErrorIs(t, err, tt.wantErrT)
			}
		})
	}
}

func Test_gitlabAPI_Validate(t *testing.T) {
	r := mux.NewRouter()
	r.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.Println(r.RequestURI)
			next.ServeHTTP(w, r)
		})
	})
	auth.GitLabStubHandlers(r,
		[]string{"123456"},
		map[string]string{},
		map[string]string{})
	ts := httptest.NewServer(r)
	defer ts.Close()
	type args struct {
		apiKey string
	}
	tests := []struct {
		name     string
		args     args
		wantErr  bool
		wantErrT error
	}{
		{
			"api key is not authorized",
			args{
				apiKey: "foo",
			},
			true,
			auth.ErrUnauthorized,
		},
		{
			"api key is valid",
			args{
				apiKey: "123456",
			},
			false,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			url, err := url.Parse(ts.URL)
			if err != nil {
				t.Fatal(err)
			}
			g := auth.NewAPI(&http.Client{}, url)
			err = g.Validate(tt.args.apiKey)
			if (err != nil) != tt.wantErr {
				t.Errorf("gitlabAPI.Validate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.wantErrT != nil {
				assert.ErrorIs(t, err, tt.wantErrT)
			}
		})
	}
}
