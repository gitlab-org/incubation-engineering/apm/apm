package auth_test

import (
	"context"
	"errors"
	"reflect"
	"testing"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/auth"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/mocks"
)

func TestAuth_Valid(t *testing.T) {
	type fields struct {
		GlAPI *mocks.GitLabAPI
	}
	type args struct {
		token string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr bool
	}{
		{
			"validate unknown error is returned",
			fields{
				func() *mocks.GitLabAPI {
					m := &mocks.GitLabAPI{}
					m.On("Validate", "foo").Return(errors.New("unknown")).Once()
					return m
				}(),
			},
			args{
				"foo",
			},
			false,
			true,
		},
		{
			"forbidden error is handled returning false",
			fields{
				func() *mocks.GitLabAPI {
					m := &mocks.GitLabAPI{}
					m.On("Validate", "foo").Return(auth.ErrForbidden).Once()
					return m
				}(),
			},
			args{
				"foo",
			},
			false,
			false,
		},
		{
			"no validate error returns true",
			fields{
				func() *mocks.GitLabAPI {
					m := &mocks.GitLabAPI{}
					m.On("Validate", "foo").Return(nil).Once()
					return m
				}(),
			},
			args{
				"foo",
			},
			true,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &auth.Auth{
				GlAPI: tt.fields.GlAPI,
			}
			got, err := a.Valid(tt.args.token)
			if (err != nil) != tt.wantErr {
				t.Errorf("Auth.Valid() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Auth.Valid() = %v, want %v", got, tt.want)
			}

			tt.fields.GlAPI.AssertExpectations(t)
		})
	}
}

func TestAuth_GetProjectInfo(t *testing.T) {
	type fields struct {
		GlAPI *mocks.GitLabAPI
	}
	type args struct {
		token         string
		projectId     string
		environmentId string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *auth.Project
		wantErr bool
	}{
		{
			"token is a required argument",
			fields{
				&mocks.GitLabAPI{},
			},
			args{},
			nil,
			true,
		},
		{
			"project id is a required parameter",
			fields{
				&mocks.GitLabAPI{},
			},
			args{
				token: "1234",
			},
			nil,
			true,
		},
		{
			"API get projet error is returned",
			fields{
				func() *mocks.GitLabAPI {
					m := &mocks.GitLabAPI{}
					m.On("GetProject", "1234", "12").Return(nil, errors.New("proj error")).Once()
					return m
				}(),
			},
			args{
				token:     "1234",
				projectId: "12",
			},
			nil,
			true,
		},
		{
			"project without environment ID is returned",
			fields{
				func() *mocks.GitLabAPI {
					m := &mocks.GitLabAPI{}
					m.On("GetProject", "1234", "12").Return(&auth.Project{
						ID: 12,
					}, nil).Once()
					return m
				}(),
			},
			args{
				token:     "1234",
				projectId: "12",
			},
			&auth.Project{
				ID: 12,
			},
			false,
		},
		{
			"environment get error is handled",
			fields{
				func() *mocks.GitLabAPI {
					m := &mocks.GitLabAPI{}
					m.On("GetProject", "1234", "12").Return(&auth.Project{
						ID: 12,
					}, nil).Once()
					m.On("GetEnvironments", "1234", "12").Return(nil, errors.New("envs error")).Once()
					return m
				}(),
			},
			args{
				token:         "1234",
				projectId:     "12",
				environmentId: "bar",
			},
			nil,
			true,
		},
		{
			"environments not matching ID returns error",
			fields{
				func() *mocks.GitLabAPI {
					m := &mocks.GitLabAPI{}
					m.On("GetProject", "1234", "12").Return(&auth.Project{
						ID: 12,
					}, nil).Once()
					m.On("GetEnvironments", "1234", "12").Return([]auth.ProjectEnvironment{
						{
							ID: 1,
						},
					}, nil).Once()
					return m
				}(),
			},
			args{
				token:         "1234",
				projectId:     "12",
				environmentId: "bar",
			},
			nil,
			true,
		},
		{
			"project with specific environment is returned if IDs match",
			fields{
				func() *mocks.GitLabAPI {
					m := &mocks.GitLabAPI{}
					m.On("GetProject", "1234", "12").Return(&auth.Project{
						ID: 12,
					}, nil).Once()
					m.On("GetEnvironments", "1234", "12").Return([]auth.ProjectEnvironment{
						{
							ID:   1,
							Name: "bar",
						},
					}, nil).Once()
					return m
				}(),
			},
			args{
				token:         "1234",
				projectId:     "12",
				environmentId: "bar",
			},
			&auth.Project{
				ID: 12,
				Environment: &auth.ProjectEnvironment{
					ID:   1,
					Name: "bar",
				},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &auth.Auth{
				GlAPI: tt.fields.GlAPI,
			}
			got, err := a.GetProjectInfo(tt.args.token, tt.args.projectId, tt.args.environmentId)
			if (err != nil) != tt.wantErr {
				t.Errorf("Auth.GetProjectInfo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Auth.GetProjectInfo() = %v, want %v", got, tt.want)
			}

			tt.fields.GlAPI.AssertExpectations(t)
		})
	}
}

func TestHash(t *testing.T) {
	hash1 := auth.Hash("token", "host")
	hash2 := auth.Hash("tok", "enhost")
	hash3 := auth.Hash("token", "host")

	assert.NotEqual(t, hash1, hash2, "hash of different parts should not be equal")
	assert.Equal(t, hash1, hash3, "hash of same parts should be equal")
}

func TestAuth_StoreSession(t *testing.T) {
	type fields struct {
		SessionDuration time.Duration
		Cache           *mocks.Redis
	}
	type args struct {
		sessionID string
		project   *auth.Project
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"session id is required",
			fields{
				Cache: &mocks.Redis{},
			},
			args{},
			true,
		},
		{
			"pipeline error is handled",
			fields{
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("Pipelined", context.TODO(), mock.Anything).Return(errors.New("pipeline error")).Once()
					return m
				}(),
			},
			args{
				sessionID: "foo",
			},
			true,
		},
		{
			"project without an environment is set",
			fields{
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("Pipelined", context.TODO(), mock.Anything).Run(func(args mock.Arguments) {
						_ = args[1].(func(pl auth.Redis) error)(m)
					}).Return(nil).Once()
					m.On("HMSet", context.TODO(), "foo", "project", "12", "environment", "").Return(nil).Once()
					return m
				}(),
			},
			args{
				sessionID: "foo",
				project: &auth.Project{
					ID: 12,
				},
			},
			false,
		},
		{
			"project with expiry duration is set",
			fields{
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("Pipelined", context.TODO(), mock.Anything).Run(func(args mock.Arguments) {
						_ = args[1].(func(pl auth.Redis) error)(m)
					}).Return(nil).Once()
					m.On("HMSet", context.TODO(), "foo", "project", "12", "environment", "").Return(nil).Once()
					m.On("Expire", context.TODO(), "foo", time.Hour).Return(nil).Once()
					return m
				}(),
				SessionDuration: time.Hour,
			},
			args{
				sessionID: "foo",
				project: &auth.Project{
					ID: 12,
				},
			},
			false,
		},
		{
			"project with environment and expiry is set",
			fields{
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("Pipelined", context.TODO(), mock.Anything).Run(func(args mock.Arguments) {
						_ = args[1].(func(pl auth.Redis) error)(m)
					}).Return(nil).Once()
					m.On("HMSet", context.TODO(), "foo", "project", "12", "environment", "1").Return(nil).Once()
					m.On("Expire", context.TODO(), "foo", time.Hour).Return(nil).Once()
					return m
				}(),
				SessionDuration: time.Hour,
			},
			args{
				sessionID: "foo",
				project: &auth.Project{
					ID: 12,
					Environment: &auth.ProjectEnvironment{
						ID: 1,
					},
				},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &auth.Auth{
				SessionDuration: tt.fields.SessionDuration,
				Cache:           tt.fields.Cache,
			}
			if err := a.StoreSession(tt.args.sessionID, tt.args.project); (err != nil) != tt.wantErr {
				t.Errorf("Auth.StoreSession() error = %v, wantErr %v", err, tt.wantErr)
			}

			tt.fields.Cache.AssertExpectations(t)
		})
	}
}

func TestAuth_GetSession(t *testing.T) {
	type fields struct {
		Cache *mocks.Redis
	}
	type args struct {
		sessionID string
	}
	tests := []struct {
		name              string
		fields            fields
		args              args
		wantProjectId     int
		wantEnvironmentId int
		wantErr           bool
		isErr             error
	}{
		{
			"session id is required",
			fields{
				Cache: &mocks.Redis{},
			},
			args{},
			0,
			0,
			true,
			nil,
		},
		{
			"hmget slice result error is handled",
			fields{
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("HMGet", context.TODO(), "foo", "project", "environment").Return(redis.NewSliceResult(nil, errors.New("result error"))).Once()
					return m
				}(),
			},
			args{
				sessionID: "foo",
			},
			0,
			0,
			true,
			nil,
		},
		{
			"hmget slice result empty returns not found",
			fields{
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("HMGet", context.TODO(), "foo", "project", "environment").Return(redis.NewSliceResult(nil, nil)).Once()
					return m
				}(),
			},
			args{
				sessionID: "foo",
			},
			0,
			0,
			true,
			auth.ErrNotFound,
		},
		{
			"hmget slice result not size 2 returns error",
			fields{
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("HMGet", context.TODO(), "foo", "project", "environment").Return(redis.NewSliceResult([]interface{}{"1", "2", "3"}, nil)).Once()
					return m
				}(),
			},
			args{
				sessionID: "foo",
			},
			0,
			0,
			true,
			nil,
		},
		{
			"project and environment id are returned successfully",
			fields{
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("HMGet", context.TODO(), "foo", "project", "environment").Return(redis.NewSliceResult([]interface{}{"12", "1"}, nil)).Once()
					return m
				}(),
			},
			args{
				sessionID: "foo",
			},
			12,
			1,
			false,
			nil,
		},
		{
			"nil project id from hmget returns not found",
			fields{
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("HMGet", context.TODO(), "foo", "project", "environment").Return(redis.NewSliceResult([]interface{}{nil, nil}, nil)).Once()
					return m
				}(),
			},
			args{
				sessionID: "foo",
			},
			0,
			0,
			true,
			auth.ErrNotFound,
		},
		{
			"non integer project id returns ",
			fields{
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("HMGet", context.TODO(), "foo", "project", "environment").Return(redis.NewSliceResult([]interface{}{"tee", nil}, nil)).Once()
					return m
				}(),
			},
			args{
				sessionID: "foo",
			},
			0,
			0,
			true,
			nil,
		},
		{
			"non int environment id returns int conversion error",
			fields{
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("HMGet", context.TODO(), "foo", "project", "environment").Return(redis.NewSliceResult([]interface{}{"12", "blah"}, nil)).Once()
					return m
				}(),
			},
			args{
				sessionID: "foo",
			},
			12,
			0,
			true,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &auth.Auth{
				Cache: tt.fields.Cache,
			}
			gotProjectId, gotEnvironmentId, err := a.GetSession(tt.args.sessionID)
			if (err != nil) != tt.wantErr {
				t.Errorf("Auth.GetSession() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotProjectId != tt.wantProjectId {
				t.Errorf("Auth.GetSession() gotProjectId = %v, want %v", gotProjectId, tt.wantProjectId)
			}
			if gotEnvironmentId != tt.wantEnvironmentId {
				t.Errorf("Auth.GetSession() gotEnvironmentId = %v, want %v", gotEnvironmentId, tt.wantEnvironmentId)
			}
			if tt.isErr != nil {
				assert.ErrorIs(t, err, tt.isErr)
			}

			tt.fields.Cache.AssertExpectations(t)
		})
	}
}

func TestAPIKey_Unpack(t *testing.T) {
	tests := []struct {
		name              string
		k                 auth.APIKey
		wantKey           string
		wantProjectId     string
		wantEnvironmentId string
		wantErr           bool
	}{
		{
			"empty key returns parts error",
			auth.APIKey("   "),
			"",
			"",
			"",
			true,
		},
		{
			"empty key parts returns error",
			auth.APIKey(":"),
			"",
			"",
			"",
			true,
		},
		{
			"key with too many parts returns error",
			auth.APIKey("a:b:c:d"),
			"",
			"",
			"",
			true,
		},
		{
			"project id cannot be empty returns error",
			auth.APIKey("a:  "),
			"a",
			"",
			"",
			true,
		},
		{
			"empty environment id is allowed",
			auth.APIKey("123456:abc:"),
			"123456",
			"abc",
			"",
			false,
		},
		{
			"all key parts picked up and returned",
			auth.APIKey("123456:abc:32"),
			"123456",
			"abc",
			"32",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotKey, gotProjectId, gotEnvironmentId, err := tt.k.Unpack()
			if (err != nil) != tt.wantErr {
				t.Errorf("APIKey.Unpack() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotKey != tt.wantKey {
				t.Errorf("APIKey.Unpack() gotKey = %v, want %v", gotKey, tt.wantKey)
			}
			if gotProjectId != tt.wantProjectId {
				t.Errorf("APIKey.Unpack() gotProjectId = %v, want %v", gotProjectId, tt.wantProjectId)
			}
			if gotEnvironmentId != tt.wantEnvironmentId {
				t.Errorf("APIKey.Unpack() gotEnvironmentId = %v, want %v", gotEnvironmentId, tt.wantEnvironmentId)
			}
		})
	}
}

func TestAuth_Verify(t *testing.T) {
	type fields struct {
		GlAPI           *mocks.GitLabAPI
		SessionDuration time.Duration
		Cache           *mocks.Redis
	}
	type args struct {
		key auth.APIKey
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *auth.Session
		wantErr bool
		isError error
	}{
		{
			"invalid api key error is returned",
			fields{
				GlAPI: &mocks.GitLabAPI{},
				Cache: &mocks.Redis{},
			},
			args{
				"potato",
			},
			nil,
			true,
			auth.ErrKeyInvalid,
		},
		{
			"get session hmget error is returned",
			fields{
				GlAPI: &mocks.GitLabAPI{},
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("HMGet", context.TODO(), auth.Hash("1234", "foo", ""), "project", "environment").
						Return(redis.NewSliceResult(nil, errors.New("hmget"))).Once()
					return m
				}(),
			},
			args{
				"1234:foo",
			},
			nil,
			true,
			nil,
		},
		{
			"get session result is returned",
			fields{
				GlAPI: &mocks.GitLabAPI{},
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("HMGet", context.TODO(), auth.Hash("1234", "foo", ""), "project", "environment").
						Return(redis.NewSliceResult([]interface{}{"12", ""}, nil)).Once()
					return m
				}(),
			},
			args{
				"1234:foo",
			},
			&auth.Session{
				Id:            auth.Hash("1234", "foo", ""),
				ProjectId:     12,
				EnvironmentId: 0,
			},
			false,
			nil,
		},
		{
			"get project info for empty session error is returned",
			fields{
				GlAPI: func() *mocks.GitLabAPI {
					m := &mocks.GitLabAPI{}
					m.On("GetProject", "1234", "foo").Return(nil, errors.New("get project")).Once()
					return m
				}(),
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("HMGet", context.TODO(), auth.Hash("1234", "foo", ""), "project", "environment").
						Return(redis.NewSliceResult(nil, nil)).Once()
					return m
				}(),
			},
			args{
				"1234:foo",
			},
			nil,
			true,
			nil,
		},
		{
			"get project info for empty session forbidden not developer returned",
			fields{
				GlAPI: func() *mocks.GitLabAPI {
					m := &mocks.GitLabAPI{}
					m.On("GetProject", "1234", "foo").Return(&auth.Project{
						ID: 12,
						Permissions: auth.ProjectPermissions{
							ProjectAccess: auth.ProjectAccess{
								AccessLevel: 0,
							},
						},
					}, nil).Once()
					return m
				}(),
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("HMGet", context.TODO(), auth.Hash("1234", "foo", ""), "project", "environment").
						Return(redis.NewSliceResult(nil, nil)).Once()
					return m
				}(),
			},
			args{
				"1234:foo",
			},
			nil,
			true,
			auth.ErrForbidden,
		},
		{
			"get project info for empty session store error is returned",
			fields{
				GlAPI: func() *mocks.GitLabAPI {
					m := &mocks.GitLabAPI{}
					m.On("GetProject", "1234", "foo").Return(&auth.Project{
						ID: 12,
						Permissions: auth.ProjectPermissions{
							ProjectAccess: auth.ProjectAccess{
								AccessLevel: 30,
							},
						},
					}, nil).Once()
					return m
				}(),
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					m.On("HMGet", context.TODO(), auth.Hash("1234", "foo", ""), "project", "environment").
						Return(redis.NewSliceResult(nil, nil)).Once()
					m.On("Pipelined", context.TODO(), mock.Anything).Return(errors.New("pipelined")).Once()
					return m
				}(),
			},
			args{
				"1234:foo",
			},
			nil,
			true,
			nil,
		},
		{
			"get project info for empty session GitLab project is returned and session stored",
			fields{
				GlAPI: func() *mocks.GitLabAPI {
					m := &mocks.GitLabAPI{}
					m.On("GetProject", "1234", "foo").Return(&auth.Project{
						ID: 12,
						Permissions: auth.ProjectPermissions{
							ProjectAccess: auth.ProjectAccess{
								AccessLevel: 30,
							},
						},
					}, nil).Once()
					m.On("GetEnvironments", "1234", "foo").Return([]auth.ProjectEnvironment{
						{
							ID:   2,
							Name: "baz",
						},
						{
							ID:   1,
							Name: "bar",
						},
					}, nil).Once()
					return m
				}(),
				Cache: func() *mocks.Redis {
					m := &mocks.Redis{}
					h := auth.Hash("1234", "foo", "bar")
					m.On("HMGet", context.TODO(), h, "project", "environment").
						Return(redis.NewSliceResult(nil, nil)).Once()
					m.On("Pipelined", context.TODO(), mock.Anything).Run(func(args mock.Arguments) {
						_ = args[1].(func(pl auth.Redis) error)(m)
					}).Return(nil).Once()
					m.On("HMSet", context.TODO(), h, "project", "12", "environment", "1").Return(nil).Once()
					return m
				}(),
			},
			args{
				"1234:foo:bar",
			},
			&auth.Session{
				Id:            auth.Hash("1234", "foo", "bar"),
				ProjectId:     12,
				EnvironmentId: 1,
			},
			false,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &auth.Auth{
				GlAPI:           tt.fields.GlAPI,
				SessionDuration: tt.fields.SessionDuration,
				Cache:           tt.fields.Cache,
			}
			got, err := a.Verify(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Auth.Verify() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Auth.Verify() = %v, want %v", got, tt.want)
			}
			if tt.isError != nil {
				assert.ErrorIs(t, err, tt.isError)
			}
			tt.fields.Cache.AssertExpectations(t)
			tt.fields.GlAPI.AssertExpectations(t)
		})
	}
}
