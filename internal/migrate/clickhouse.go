// migrate package modifies the github.com/golang-migrate/migrate/blob/master/database/clickhouse migrator for ease of use.
// Takes the default config and a standard ClickHouse DSN rather than the migrate driver format so it's easier to share the DSN value.
package migrate

import (
	"bytes"
	"database/sql"
	"fmt"
	"io"

	_ "github.com/ClickHouse/clickhouse-go"
	"github.com/golang-migrate/migrate/v4/database"
	"github.com/golang-migrate/migrate/v4/database/clickhouse"
)

type driver struct {
	database.Driver
	config *clickhouse.Config
}

// NewClickHouseDriver creates a ClickHouse migrations driver with config values
func NewClickHouseDriver(dsn string, config *clickhouse.Config) (database.Driver, error) {
	conn, err := sql.Open("clickhouse", dsn)
	if err != nil {
		return nil, fmt.Errorf("clickhouse connection open: %w", err)
	}

	d, err := clickhouse.WithInstance(conn, config)
	if err != nil {
		return nil, fmt.Errorf("clickhouse setup: %w", err)
	}
	return &driver{
		d,
		config,
	}, nil
}

// Run overrides default Run to provide some basic variable substitution.
// $DATABASE and $CLUSTER strings are replaced by the values in config.
func (d *driver) Run(r io.Reader) error {
	bs, err := io.ReadAll(r)
	if err != nil {
		return fmt.Errorf("driver reader: %w", err)
	}
	bs = bytes.ReplaceAll(bs, []byte("$DATABASE"), []byte(d.config.DatabaseName))
	bs = bytes.ReplaceAll(bs, []byte("$CLUSTER"), []byte(d.config.ClusterName))

	if err := d.Driver.Run(bytes.NewReader(bs)); err != nil {
		return fmt.Errorf("clickhouse driver run: %w", err)
	}
	return nil
}
