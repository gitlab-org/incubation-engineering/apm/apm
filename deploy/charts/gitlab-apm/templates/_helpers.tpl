{{/*
Expand the name of the chart.
*/}}
{{- define "gitlab-apm.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "gitlab-apm.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "gitlab-apm.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "gitlab-apm.labels" -}}
helm.sh/chart: {{ include "gitlab-apm.chart" . }}
{{ include "gitlab-apm.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "gitlab-apm.selectorLabels" -}}
app.kubernetes.io/name: {{ include "gitlab-apm.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the gateway service account to use
*/}}
{{- define "gitlab-apm.gateway.serviceAccountName" -}}
{{- if .Values.gateway.serviceAccount.create }}
{{- default (print (include "gitlab-apm.fullname" .) "-gateway") .Values.gateway.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.gateway.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Gateway metadata name for resources
*/}}
{{- define "gitlab-apm.gateway.name" -}}
{{- include "gitlab-apm.fullname" . }}-gateway
{{- end }}

{{/*
Create the name of the migrations service account to use
*/}}
{{- define "gitlab-apm.migrations.serviceAccountName" -}}
{{- if .Values.migrations.serviceAccount.create }}
{{- default (print (include "gitlab-apm.fullname" .) "-migrations") .Values.migrations.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.migrations.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Return the appropriate apiVersion for rbac.
*/}}
{{- define "gitlab-apm.rbac.apiVersion" -}}
{{- if .Capabilities.APIVersions.Has "rbac.authorization.k8s.io/v1" }}
{{- print "rbac.authorization.k8s.io/v1" -}}
{{- else -}}
{{- print "rbac.authorization.k8s.io/v1beta1" -}}
{{- end -}}
{{- end -}}

{{/*
Gtlb-stub metadata name for resources
*/}}
{{- define "gitlab-apm.gtlb-stub.name" -}}
{{- include "gitlab-apm.fullname" . }}-gtlb-stub
{{- end }}

{{/*
ClickHouse DSN for gateway based on values and enabled ClickHouse installation
*/}}
{{- define "gitlab-apm.gateway.clickHouseDSN" -}}
{{- if .Values.gateway.clickHouseDSN }}
{{- .Values.gateway.clickHouseDSN }}
{{- else }}
{{- with .Values.gateway -}}
{{- include "gitlab-apm.clickHouseDSN" 
    (dict 
        "servers" $.Values.clickHouseServers
        "database" $.Values.apmDatabase
        "username" .clickHouseUsername
        "password" .clickHousePassword
        "debug" $.Values.development) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Migrator DSN settings based on values and enabled ClickHouse installation
*/}}
{{- define "gitlab-apm.migrations.clickHouseDSN" -}}
{{- if .Values.migrations.clickHouseDSN }}
{{- .Values.migrations.clickHouseDSN }}
{{- else }}
{{- with .Values.migrations -}}
{{- include "gitlab-apm.clickHouseDSN" 
    (dict 
        "servers" $.Values.clickHouseServers
        "database" $.Values.apmDatabase
        "username" .clickHouseUsername
        "password" .clickHousePassword) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Build ClickHouse DSN from common args
*/}}
{{- define "gitlab-apm.clickHouseDSN" -}}
tcp://{{ mustFirst .servers }}?database={{ .database }}&username={{ .username }}&password={{ .password }}
{{- $althosts := rest .servers }}
{{- if $althosts -}}
&alt_hosts={{- join "," $althosts }}
{{- end }}
{{- if .debug -}}
&debug=true
{{- end }}
{{- end }}