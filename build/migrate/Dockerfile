ARG GO_VERSION
FROM golang:${GO_VERSION}-alpine as build

WORKDIR /build

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY cmd/ cmd/
COPY internal/ internal/
RUN CGO_ENABLED=0 go build -ldflags="-extldflags=-static" ./cmd/migrate

FROM alpine:latest

RUN apk add --no-cache curl

# install and verify kubectl
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256" && \
    (echo "$(cat kubectl.sha256)  kubectl" | sha256sum -c -) && \
    chmod +x kubectl && \
    mv kubectl /usr/local/bin/kubectl

COPY --from=build /build/migrate /usr/local/bin/migrate

COPY scripts/migrations /migrations/
COPY scripts/migrate/* /usr/local/bin/