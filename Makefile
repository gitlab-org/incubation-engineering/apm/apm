# minikube/helm settings
DEFAULT_NAMESPACE=default
HELM_RELEASE=gitlab-apm-dev
HELM_CHART_PATH=deploy/charts/gitlab-apm
MINIKUBE_NAME=gitlab-apm
# k8s version for minikube ideally same as production GKE
K8S_VERSION=v1.20.11

# build/deploy settings
export GO_VERSION=1.16.8
SKAFFOLD_YAML=deploy/skaffold.yaml

# migrations settings
MIGRATIONS_PATH=scripts/migrations
MIGRATIONS_DATABASE=apm
MIGRATOR_COMMAND=go run ./cmd/migrate

# integration test settings
export TEST_CLICKHOUSE_DSN?=tcp://localhost:9000?database=apm

# CI settings
CI?=false

.PHONY: build
build:
	-mkdir bin
	go build -o ./bin ./...

.PHONY: minikube-start
# start minikube and add the ClickHouse operator
# api-server port 6443 must be set to allow Datadog to work
minikube-start:
	minikube start -p ${MINIKUBE_NAME} --disk-size 8Gi --kubernetes-version ${K8S_VERSION} --addons metrics-server --apiserver-port 6443
	-curl -s https://raw.githubusercontent.com/Altinity/clickhouse-operator/master/deploy/operator-web-installer/clickhouse-operator-install.sh | OPERATOR_NAMESPACE=${DEFAULT_NAMESPACE} bash

.PHONY: minikube-stop
# stop the minikube cluster
minikube-stop:
	minikube stop -p ${MINIKUBE_NAME}

.PHONY: minikube-delete
# delete the minikube cluster
minikube-delete:
	minikube delete -p ${MINIKUBE_NAME}

.PHONY: helm-build
helm-build:
	helm dep build ${HELM_CHART_PATH}

.PHONY: helm-migrations
# migrations need to be copied into the chart directory, unfortunately symlinks don't work
helm-migrations:
	-rm -r ${HELM_CHART_PATH}/migrations
	cp -Tr ${MIGRATIONS_PATH} ${HELM_CHART_PATH}/migrations

.PHONY: helm-lint
helm-lint: helm-migrations
	helm lint ${HELM_CHART_PATH}

.PHONY: helm-install
helm-install: helm-migrations
	helm install ${HELM_RELEASE} ${HELM_CHART_PATH}

.PHONY: helm-upgrade
helm-upgrade: helm-migrations
	helm upgrade ${HELM_RELEASE} ${HELM_CHART_PATH}

.PHONY: skaffold-run
skaffold-run: helm-migrations
	skaffold run -f ${SKAFFOLD_YAML}

.PHONY: skaffold-delete
skaffold-delete: helm-migrations
	skaffold delete -f ${SKAFFOLD_YAML}
	
.PHONY: migrate-version
# get version of migration from the database
migrate-version:
	${MIGRATOR_COMMAND} --dsn="${TEST_CLICKHOUSE_DSN}" --database="${MIGRATIONS_DATABASE}" --path="${MIGRATIONS_PATH}" version

.PHONY: migrate-create
migrate-create: NAME=CHANGE_ME
migrate-create:
	go run github.com/golang-migrate/migrate/v4/cmd/migrate create -ext sql -dir ${MIGRATIONS_PATH} ${NAME}

.PHONY: migrate-up
migrate-up:
	${MIGRATOR_COMMAND} --dsn="${TEST_CLICKHOUSE_DSN}" --database="${MIGRATIONS_DATABASE}" --path="${MIGRATIONS_PATH}" up

.PHONY: test-migrations
test-migrations:
	./scripts/test_migrations.sh

.PHONY: generate-mocks
generate-mocks:
	go run github.com/vektra/mockery/v2 --all --output ./internal/mocks

.PHONY: test
test:
	go test -tags=none -race -count 1 -cover ./...

.PHONY: integration-test
integration-test:
	@if [ "$$CI" != "true" ]; then\
		docker-compose up -d;\
	fi
	go test -v -run .*Integration -tags integration -count 1 -race ./...

.PHONY: lint
lint:
	go run github.com/golangci/golangci-lint/cmd/golangci-lint run