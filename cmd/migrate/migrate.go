package main

import (
	"errors"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	migrateV4 "github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database"
	"github.com/golang-migrate/migrate/v4/database/clickhouse"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/migrate"
	"gitlab.com/gitlab-org/labkit/log"
)

func main() {
	logFlush, err := log.Initialize(log.WithFormatter("json"), log.WithOutputName("stderr"))
	if err != nil {
		log.WithError(err).Fatal("log init")
	}
	defer logFlush.Close()

	var migrater *migrateV4.Migrate

	app := &cli.App{
		Description: "Wrapper around github.com/golang-migrate/migrate for ease of use in GitLab APM.",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "dsn",
				Required: true,
				Usage:    "DSN for clickhouse-go",
				EnvVars:  []string{"MIGRATE_DSN"},
			},
			&cli.PathFlag{
				Name:     "path",
				Required: true,
				Usage:    "path to migrations folder",
				EnvVars:  []string{"MIGRATE_PATH"},
			},
			&cli.StringFlag{
				Name:     "database",
				Required: true,
				Usage:    "database where migrations are stored",
				EnvVars:  []string{"MIGRATE_DATABASE"},
			},
			&cli.StringFlag{
				Name:    "cluster",
				Usage:   "cluster name to store replicated migrations against",
				Value:   "`{cluster}`",
				EnvVars: []string{"MIGRATE_CLUSTER"},
			},
			&cli.BoolFlag{
				Name:    "multi-statement",
				Usage:   "allow sql files to contain multiple statements separated by semi-colon",
				Value:   true,
				EnvVars: []string{"MIGRATE_MULTI_STATEMENT"},
			},
			&cli.StringFlag{
				Name:    "table-engine",
				Usage:   "table engine for migrations table",
				Value:   "ReplicatedMergeTree",
				EnvVars: []string{"MIGRATE_TABLE_ENGINE"},
			},
			&cli.StringFlag{
				Name:    "table",
				Usage:   "migrations table name",
				Value:   clickhouse.DefaultMigrationsTable,
				EnvVars: []string{"MIGRATE_TABLE"},
			},
		},
		Before: func(c *cli.Context) error {
			d, err := driver(c)
			if err != nil {
				return err
			}
			p := fmt.Sprintf("file://%v", c.Path("path"))
			migrater, err = migrateV4.NewWithDatabaseInstance(p, "clickhouse", d)
			if err != nil {
				return fmt.Errorf("migrator setup: %w", err)
			}

			// migrater defaults taken from migrate cli
			migrater.Log = logger{}
			migrater.LockTimeout = time.Second * 15
			migrater.PrefetchMigrations = 10
			// handle Ctrl+c
			signals := make(chan os.Signal, 1)
			signal.Notify(signals, syscall.SIGINT, os.Interrupt)
			go func() {
				for range signals {
					log.Info("Stopping after this running migration ...")
					migrater.GracefulStop <- true
					return
				}
			}()

			return nil
		},
		After: func(c *cli.Context) error {
			if migrater != nil {
				if _, err := migrater.Close(); err != nil {
					return fmt.Errorf("migrater close: %w", err)
				}
			}
			return nil
		},
		Commands: []*cli.Command{
			{
				Name:      "goto",
				Usage:     "Migrate to specific version",
				ArgsUsage: "V",
				Action: func(c *cli.Context) error {
					v := c.Args().First()
					if v == "" {
						return errors.New("goto requires one argument")
					}

					n, err := strconv.ParseUint(v, 10, 64)
					if err != nil {
						return fmt.Errorf("can't read version argument V: %w", err)
					}

					if err := migrater.Migrate(uint(n)); err != nil {
						return migrateError(err)
					}

					return nil
				},
			},
			{
				Name:  "up",
				Usage: "Apply all up migrations",
				Action: func(c *cli.Context) error {
					if err := migrater.Up(); err != nil {
						return migrateError(err)
					}
					return nil
				},
			},
			{
				Name:  "down",
				Usage: "Apply all down migrations",
				Action: func(c *cli.Context) error {
					if err := migrater.Down(); err != nil {
						return migrateError(err)
					}
					return nil
				},
			},
			{
				Name:  "version",
				Usage: "Print current migration version",
				Action: func(c *cli.Context) error {
					v, dirty, err := migrater.Version()
					if err != nil {
						return fmt.Errorf("version: %w", err)
					}
					log.WithFields(log.Fields{
						"dirty":   dirty,
						"version": v,
					}).Info("version")

					fmt.Println(v)
					return nil
				},
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.WithError(err).Fatal("shut down")
	}
}

func driver(c *cli.Context) (database.Driver, error) {
	d, err := migrate.NewClickHouseDriver(c.String("dsn"), &clickhouse.Config{
		DatabaseName:          c.String("database"),
		ClusterName:           c.String("cluster"),
		MigrationsTable:       c.String("table"),
		MigrationsTableEngine: c.String("table-engine"),
		MultiStatementEnabled: c.Bool("multi-statement"),
		MultiStatementMaxSize: clickhouse.DefaultMultiStatementMaxSize,
	})
	if err != nil {
		return nil, fmt.Errorf("new driver: %w", err)
	}
	return d, nil
}

type logger struct{}

func (l logger) Printf(format string, v ...interface{}) {
	log.Info(fmt.Sprintf(format, v...))
}

func (l logger) Verbose() bool {
	return false
}

func migrateError(err error) error {
	if err != migrateV4.ErrNoChange {
		return fmt.Errorf("migrate: %w", err)
	}
	log.Info(err)
	return nil
}
