package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/ClickHouse/clickhouse-go"
	"github.com/go-redis/redis/v8"
	"github.com/gorilla/mux"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/auth"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/datadog"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/metrics"
	"gitlab.com/gitlab-org/labkit/log"
)

func main() {
	app := &cli.App{
		Description: "Gateway server for APM data",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "listen",
				Usage:   "address and port to listen",
				EnvVars: []string{"GW_LISTEN"},
				Value:   ":5678",
			},
			&cli.StringFlag{
				Name:     "clickhouse-dsn",
				Usage:    "clickhouse DSN string",
				Required: true,
				EnvVars:  []string{"GW_CLICKHOUSE_DSN"},
			},
			&cli.BoolFlag{
				Name:    "development",
				Aliases: []string{"dev"},
				Usage:   "development mode for logging etc",
				EnvVars: []string{"GW_DEVELOPMENT"},
				Value:   false,
			},
			&cli.StringFlag{
				Name:  "log-level",
				Value: "info",
				Usage: "log level, set to debug if development flag set",
			},
			&cli.DurationFlag{
				Name:    "graceful-timeout",
				Usage:   "graceful shutdown timeout for the server",
				EnvVars: []string{"GW_GRACEFUL_TIMEOUT"},
				Value:   time.Second * 15,
			},
			&cli.StringFlag{
				Name:    "gitlab-url",
				Aliases: []string{"gitlab"},
				Usage:   "GitLab base url for API access",
				EnvVars: []string{"GW_GITLAB_URL", "GW_GITLAB"},
				Value:   "https://gitlab.com",
			},
			&cli.StringFlag{
				Name:    "redis-address",
				Aliases: []string{"redis, redis-addr"},
				Usage:   "Redis address for session storage",
				EnvVars: []string{"GW_REDIS_ADDRESS", "GW_REDIS"},
				Value:   "localhost:6379",
			},
			&cli.StringFlag{
				Name:    "redis-password",
				Usage:   "Optional redis password",
				EnvVars: []string{"GW_REDIS_PASSWORD"},
				Value:   "",
			},
		},
		Action: func(c *cli.Context) error {
			dev := c.Bool("development")
			var logFlush io.Closer
			var logErr error
			if dev {
				logFlush, logErr = log.Initialize(log.WithFormatter("color"), log.WithLogLevel("debug"))
			} else {
				logFlush, logErr = log.Initialize(log.WithFormatter("json"), log.WithLogLevel(c.String("log-level")))
			}
			if logErr != nil {
				log.WithError(logErr).Fatal("log init")
			}
			defer logFlush.Close()

			db, auth, err := dependencies(
				c.String("clickhouse-dsn"),
				c.String("gitlab-url"),
				c.String("redis-address"),
				c.String("redis-password"))
			if err != nil {
				log.WithError(err).Fatal("dependencies init")
			}

			return serve(
				c.String("listen"),
				c.Duration("graceful-timeout"), db, auth)
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.WithError(err).Fatal("shut down")
	}
}

func dependencies(dsn, gitlab, redisAddress, redisPwd string) (metrics.Db, *auth.Auth, error) {
	db, err := metrics.NewDb(dsn)
	if err != nil {
		return nil, nil, fmt.Errorf("new db: %w", err)
	}

	glURL, err := url.Parse(gitlab)
	if err != nil {
		return nil, nil, fmt.Errorf("gitlab url parse: %w", err)
	}

	glClient := &http.Client{
		Timeout: time.Second * 10,
	}

	au := &auth.Auth{
		GlAPI:           auth.NewAPI(glClient, glURL),
		SessionDuration: time.Hour,
		Cache: auth.NewCache(redis.NewClient(&redis.Options{
			Addr:     redisAddress,
			Password: redisPwd,
		})),
	}

	return db, au, nil
}

func serve(listen string, gracefulTimeout time.Duration, db metrics.Db, auth *auth.Auth) error {

	r := mux.NewRouter()

	srv := &http.Server{
		Addr:         listen,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r,
	}

	r.Use(loggingMiddleware)
	r.HandleFunc("/health", HealthHandler(db, auth.Cache)).Methods("GET").Name("health")
	datadog.ApiHandlers(r,
		metrics.NewStore(db),
		auth)

	go func() {
		if err := srv.ListenAndServe(); err != http.ErrServerClosed {
			log.WithError(err).Fatal("listen")
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	<-c

	ctx, cancel := context.WithTimeout(context.Background(), gracefulTimeout)
	defer cancel()

	log.Info("shutting down")
	if err := srv.Shutdown(ctx); err != nil {
		return fmt.Errorf("server shutdown: %w", err)
	}
	return nil
}

// HealthHandler is used to check the health of the application
// 200 status code will be reported on healthy check
func HealthHandler(db metrics.Db, cache auth.Redis) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := db.Ping(); err != nil {
			log.WithError(err).Error("db ping")
			http.Error(w, "health check error", http.StatusInternalServerError)
			return
		}

		if err := cache.Ping(context.TODO()).Err(); err != nil {
			log.WithError(err).Error("redis ping")
			http.Error(w, "health check error", http.StatusInternalServerError)
			return
		}

		fmt.Fprintln(w, "OK")
	}
}

func loggingMiddleware(next http.Handler) http.Handler {
	return log.AccessLogger(next)
}
