// GitLab REST API stub for local testing

package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/gitlab-org/incubation-engineering/apm/apm/internal/auth"
	"gitlab.com/gitlab-org/labkit/log"
)

const (
	// #nosec - hardcoded for demo purposes
	apiToken1 = "123456"
)

func main() {
	logFlush, err := log.Initialize(log.WithFormatter("json"))
	if err != nil {
		log.WithError(err).Fatal("log init")
	}
	defer logFlush.Close()

	r := mux.NewRouter()
	r.Use(loggingMiddleware)

	srv := &http.Server{
		Addr:         ":8080",
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r,
	}

	auth.GitLabStubHandlers(r,
		[]string{apiToken1},
		map[string]string{
			"1":                              project1,
			"diaspora/diaspora-project-site": project1,
		},
		map[string]string{
			"1":                              environments1,
			"diaspora/diaspora-project-site": environments1,
		})

	go func() {
		if err := srv.ListenAndServe(); err != http.ErrServerClosed {
			log.WithError(err).Fatal("listen")
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	<-c

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	log.Info("shutting down")
	if err := srv.Shutdown(ctx); err != nil {
		log.WithError(err).Error("shutting down")
	}
}

func loggingMiddleware(next http.Handler) http.Handler {
	return log.AccessLogger(next)
}
