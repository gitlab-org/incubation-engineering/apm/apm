#!/bin/sh
# script for migration used in the "migrate" image
# this is an alpine image with a bourne shell
# migrate down to a version we find in the revision configmap

dsn=$MIGRATE_DSN
path=$MIGRATE_PATH
database=$MIGRATE_DATABASE
labels=$MIGRATE_LABELS

# command line overrides
while true; do
    case "$1" in
        -dsn)
            dsn=$2; shift 2 ;;
        -path)
            path=$2; shift 2 ;;
        -database)
            database=$2; shift 2 ;;
        -labels)
            labels=$2; shift 2 ;;
        "") break; ;;
        *) break; ;;
    esac
done

set -e

configmap="$(kubectl get configmaps -l "$labels" -o name --sort-by '{.metadata.labels.migration-state-revision}' | tail -n 1)"
if [ -z "$configmap" ]; then
    echo "ConfigMap for labels $labels not found, assuming no migrations to downgrade"
    exit
fi

echo "Using configmap migration state in $configmap"

version="$(kubectl get $configmap -o template='{{ .data.migrated_from }}')"
if [ -z "$version" ]; then
    echo "migrated_from in $configmap is empty, assuming no migrations to downgrade"
    exit
fi

echo "Using migrations stored at '$path'"
echo "Migrating to $version"

migrate --dsn "$dsn" --database "$database" --path "$path" goto "$version"

kubectl delete "$configmap"