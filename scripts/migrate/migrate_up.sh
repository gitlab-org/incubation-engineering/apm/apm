#!/bin/sh
# script for migration used in the "migrate" image
# this is an alpine image with a bourne shell
# run migration and save migrated version in existing configmap

dsn=$MIGRATE_DSN
path=$MIGRATE_PATH
database=$MIGRATE_DATABASE
configmap=$MIGRATE_CONFIGMAP

# command line overrides
while true; do
    case "$1" in
        -dsn)
            dsn=$2; shift 2 ;;
        -path)
            path=$2; shift 2 ;;
        -database)
            database=$2; shift 2 ;;
        -configmap)
            configmap=$2; shift 2 ;;
        "") break; ;;
        *) break; ;;
    esac
done

echo "Using migrations stored at '$path'"

migrated_from="$(migrate --dsn "$dsn" --database "$database" --path "$path" version)"
if [ $? -ne 0 ]; then
    echo "No existing migrations found"
    migrated_from=""
fi
echo "Migrating from $migrated_from"

set -e

kubectl patch configmap/"$configmap" --type merge -p '{"data":{"migrated_from":"'"$migrated_from"'"}}'

echo "Migrations running..."
migrate --dsn "$dsn" --database "$database" --path "$path" up

migrated_to="$(migrate --dsn "$dsn" --database "$database" --path "$path" version)"
echo "Migrated to $migrated_to"
kubectl patch configmap/"$configmap" --type merge -p '{"data":{"migrated_to":"'"$migrated_to"'"}}'