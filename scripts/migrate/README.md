# Migrate cluster scripts

These scripts are used along with the `migrate` image to handle migration state and rollbacks in Kubernetes and via Helm.

See [migrations docs](../../docs/migrations.md) for more info about this.