CREATE TABLE metrics_local ON CLUSTER `{cluster}`
(
    timestamp        DateTime('UTC') CODEC(DoubleDelta, LZ4),
    host             LowCardinality(String),
    measurement      LowCardinality(String),
    project_id       Int64,
    environment_id   Int64,
    `fields.name`    Array(LowCardinality(String)),
    `fields.value`   Array(Float64) CODEC(Gorilla, LZ4),
    tags Nested(
        key LowCardinality(String),
        value LowCardinality(String)
    )
) ENGINE = ReplicatedMergeTree
PARTITION BY toYYYYMMDD(timestamp)
ORDER BY (project_id, environment_id, measurement, host, timestamp);