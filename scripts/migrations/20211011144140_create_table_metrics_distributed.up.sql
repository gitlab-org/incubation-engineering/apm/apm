CREATE TABLE metrics ON CLUSTER `{cluster}` AS $DATABASE.metrics_local
ENGINE = Distributed(`{cluster}`, $DATABASE, metrics_local);