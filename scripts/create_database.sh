#!/usr/bin/env bash

set -e
clickhouse-client -mn <<- 'EOSQL'
    CREATE DATABASE IF NOT EXISTS apm ON CLUSTER `{cluster}`;
EOSQL