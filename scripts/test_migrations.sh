#!/usr/bin/env sh

MIGRATOR_CLICKHOUSE_HOST=${MIGRATOR_CLICKHOUSE_HOST:-localhost}
MIGRATOR_COMMAND=${MIGRATOR_COMMAND:-go run ./cmd/migrate}
MIGRATIONS_DSN=${MIGRATIONS_DSN:-tcp://${MIGRATOR_CLICKHOUSE_HOST}:9000?database=apm}
MIGRATIONS_PATH=${MIGRATIONS_PATH:-scripts/migrations}
MIGRATIONS_DATABASE=${MIGRATIONS_DATABASE:-apm}

set -e

# run up migrations, all down migrations, then all up to cover reversability

echo "Migrating all up..."
${MIGRATOR_COMMAND} --dsn="${MIGRATIONS_DSN}" --database="${MIGRATIONS_DATABASE}" --path="${MIGRATIONS_PATH}" up

echo "Migrating all down..."
${MIGRATOR_COMMAND} --dsn="${MIGRATIONS_DSN}" --database="${MIGRATIONS_DATABASE}" --path="${MIGRATIONS_PATH}" down

echo "Migrating all up again..."
${MIGRATOR_COMMAND} --dsn="${MIGRATIONS_DSN}" --database="${MIGRATIONS_DATABASE}" --path="${MIGRATIONS_PATH}" up